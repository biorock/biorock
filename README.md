# biorock

//__________________________

serve html page using gitlab.io

1. make a GROUP biorock
2. make in GROUP biorock a PROJECT ( repository )  biorock
3. rename in PROJECT:settings new project biorock to biorock.gitlab.io
4. make/copy the .gitlab-ci.yml file
5. start CI/CD pipeline, if running wait...
6. call it: https://biorock.gitlab.io/biorock/

( release 0.1 )  
//__________________________  

copy the p5-template  
( rev 0.2 )  
//__________________________  

03.07.2020 start work here online:  
already change in index.html and DOM.js  
so you can have a company header over the processing canvas  
( see also new dir /images/ )  
please test how this page could be incorporated into a CMS like WordPress  
( release 0.3 )  
//__________________________  
add info file in branch kllsamui_test_from_PC  

( after merge the change from that branch make:)

( rev 0.4 )  
//__________________________  

// work local RPI3 under cloneGITLAB : GIT fetch pull update to have same as online

copy files from nodeserver project code rev 0.9.6 beta  
and push up / tag revision / release 05  
( 6. ) now must call it by: https://biorock.gitlab.io/biorock/biorock.html  

//__________________________  
25.07.2020
thanks to the team for testing,
we stay in rev 0.9.6 and go from BETA to MOD, also not need a release of this.
 
info: once the changes are commited to master, you see them directly in the HTML site!

ISSUE 4: depth error 
sketch_biorock.js and DOM_code.js change site distance vars to float

ISSUE 6 & 7: disable PTZ keyboard operation 
in PTZ.js and ducu.js

ISSUE 3: separate datasheet/BOM from help texts  
make new var help by key [h] with default: false  

MOD: when you get / use the download file only  
it is difficult to make a other design from there, as you not easy see the DATA GRID settings.  
so we make a new docu.js string, what is included in download file only  
what prints the spread sheet area settings first, but only after data update!  

//__________________________  
in the help text there is already a link to a external tool,  
* for BOLPS i did a excel sheet with the KIRCHHOFF quadratic equation.  
https://docs.google.com/spreadsheets/d/1hgasQUpGNY2VDT27Jdus3ygX51Gic_MpCwbkact9KCI/edit?usp=sharing  
( same ( questionable ) math is used by the JS calculation. )  


now we have also 2 more external tools to help you:  
a spreadsheet with data and spline prepared for the tricky  
* var euro_cable  
research and tuning:  
https://docs.google.com/spreadsheets/d/1_HD6Wptpjaa5jq1LMS5tVJPnxm7R4BdLemsMBC2Aqcg/edit#gid=0  

MANUAL:  
* add info what are not in the online help [h] , thanks @thcmm  
https://globalcoral-my.sharepoint.com/:w:/g/personal/tsarkisian_globalcoral_org/EblVuMRKP8lOsBW2kLUgcRABN5ys1gwEn6uAR9bPRSshWw?rtime=MBaIvD012Eg  

//__________________________  

08.08.2020  0.9.6 MOD2  
add in header line a (translate) link to google translate english to spanish...  
add combine several docu help text changes from TPS ( copy from branch & word document ) but not all deletions  

//__________________________  

10.08.2020 0.9.6 MOD3  
code TPS idea markdown for ## to < h2 >header< /h2 > manually  
11.08.2020 0.9.6 MOD3b  
use markdown lib remarkable  
13.08.2020 0.9.6 MOD4  
add option buttons for text view options HELP / BOLPS / DOWNLOAD  

//__________________________  

18.08.2020 0.9.6 MOD4b  
help text biorock model  

//__________________________  

20.08.2020 0.9.7.1  
still beta as no feedback, so still unconfirmed steel/electric/euro calc  
add a project selector dropdown  

//__________________________  

10.09.2020 0.9.7.2
euro tuning by Aitor for steel electric

//__________________________  

19.09.2020 0.9.7.2b
BOLPS text

//__________________________  
06.10.2020 0.9.7.3a
BOLPS calc DC cable select and cable length select
the way BOLPS was implemented into the noBOLPS version was inconsistent
as DC cable suddenly are AC cables.. and new DC cable with fixed length and type are added.
so need a new concept.

//__________________________  
04.12.2020 0.9.7.4
after the JAMAICA measurements with the new RIBBON anodes a alternative BIOROCK MODEL tuning is required
instead making the settings free tunable i decide to make a selector

also add a new project for seletion: GRID-BOLPS simulation ( what not uses BOLPS so it is clampt to the beach / bad show )
and moved the cable selector right to the cable input field

//__________________________  
20.12.2020 0.9.7.4b
add noBOLPS ASCII circuit drawing
new settings for GRID_BOLPSbox simulation ( very short beach project ) defaults

//__________________________  
04.01.2021 0.9.7.4c
add bigger cable for selection

//__________________________  
22.03.2021 0.9.7.5
add even bigger cable for selection
and do a price estimation for the cables acc in a external spreadsheet
https://docs.google.com/spreadsheets/d/1EsLtppHuMNPtk8k4-ief6xzXzm0E1ke95HRuKUTZGeQ/edit?usp=sharing
results in 0.269 euro/m/mm2 copper crosssection
used for new tuning

//__________________________  
31.3.2021 0.9.7.5b
small repair text / 230V /

//__________________________  
1.4.2021 0.9.7.6
GRID BOLPS SIMULATION use BOLPScalc with a 
e_BOLPSSELV_sec_V = 230; //48.0; OVERWRITE
so not need a selector...SELV / GRID BOLPS

also add the questionable ICCP Anode model selector 
Biorock Volt = 0 + 0.216ohm * I
//__________________________  
22.4.2021 0.9.7.6b
add new even more questionable model:
Elsyca ?with 16m layer thickness? 16V at 50A
Biorock Volt = 0 + 0.320ohm * I

project 3 GRID-BOLPS with ( 10 + 6 ) 16m DC cable 25mm2 // and ( 2 * 185 ) 370m 6mm2 AC cable

twist tunnel design for 25m2 surface / 440kg steel / 9 * 4 * 3 w*l*h [m] about 50A


//__________________________  
