// file: sketch_biorock.js
// needs:
// biorock.html / now get the p5.min.js ( 1.0.0) from internet
// DOM_code.js is required for operation / data grid
// PTZ.js processing 3D show
// docu.js for project and code ( auto ) documentation

// also see file read.me and todo

let revv = 'rev 0.9.7.6b (beta)';
let revr = 'this is a evaluation version, all calculations regarding steel and electric and costs are unconfirmed';
let copyleft = '[KLL engineering](http://kll.engineering-news.org/kllfusion01/) / developed special for my friends from [BIOROCK](https://www.globalcoral.org/)(R) \nKLL [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) - 6/2020 '

let info = '',infos=''; //_____ operation and setup info
let infod = ''; //_____________ add a new string what allows the spreadsheet area to be printed ( but for download file only )
let ds='',dss='',dse=''; //____ see docu.js datasheet / steel / electric
let dllist = ''; //____________ the docu string in form for a file 
let justnow = ''; //___________ date time string for docu & filename
let e_public = true;//false;//_ show all design details ( like the electric calculation parameters and report...) or hide or reenable by URL parameter see DOM_code.js
let diagSetup = true; //_______ print a setup info for cuser of this code project, disable later
let diagc = false; //__________ print calc info to browser console..........
let diaghtml = true; //________ print calc info to browser page paragraph...
let autodl = false;//true; //__ print calc info to download file at every change
let onedl = false;//true; //___ key [d] or option click to download once ( but actually unused )
let help = false;//true; //____ verbose help info texts only after use key [h]

let show_land = true; //false; //______ show land and water level / use keyboard [s] to disable
let show_steel = false; //_____ show arc steel or weld points / use keyboard [a] to enable
let tunnelswitch = false; //___ show tunnel to beach or parallel beach / use keyboard [o] to enable

var cnv; //____________________ canvas pointer

var roddiam = 0.000; //________ [m] building steel usually comes as 9 mm or 12 mm diameter rods
var rod_a_sup = 0.0; //________ [m] add arc support steel meter
var rodm = 0.0; //_____________ [m] calculated total rod length
var rodA = 0.0; //_____________ [m2] calculated steel surface area
var rodV = 0.0; //_____________ [m3] calculated steel volume
let specweight = 7850.0; //____ [kg/m3] specific weight steel
var rodM = 0.0; //_____________ [kg] calculated steel weight ( spec mass )

var arcrad = 0.0; //___________ [m] calculated arc radius
var X=0; //____________________ [m] radius minus height ( ground to circle center )
var arcPHI = 0.0; //___________ [rad] calculated arc angle
var arclength = 0.0; //________ [m] calculated 
var archeight = 0.0; //________ [m] TUNNEL HEIGHT
var arcbase = 0.0; //__________ [m] TUNNEL WIDE // kll play to get close to 15A 5V biorock BOLPS info
var arcbaseN = 0; //___________ # number base support of arc ? every 3rd arc ?
var arcN = 1; //_______________ # number of arcs, as arc support at 1,4,7,10 .. 22 that is best
var longsN = 0; // ____________ # calculated to form grid squares by arc_dist
var arc_dist = 0.000; //_______ [m] time arcN makes the tunnel length
var tunnellength = 0; //_______ [m] TUNNEL LENGTH calculated by dist and arc numbers

var awps = []; //______________ array of vectors arc weld points
var wpv;// = createVector(0,0,0);

var p_PS_beach = 0.0; //_______ [m] use PS in beach box dist from beach
var p_beach_struct = 0.0; //__ _[m] place tunnel in water from beach
var p_struct_depth = 3.0; //___ [m] water deep ( used for calc beach / waterline slope )
var p_slope = 0.02; //_________ angle water / land calculated by structure position / depth
var p_struct_anode = 0.0; //___ [m] anodes far out for beach protect projects

var e_bamp = 0.0; //___________ [A DC] ampere per steel surface square meter
var e_anode_cable = 0; //______ [m]
var e_cathode_cable = 0; //____ [m]
var e_DC_cable = 0; //_________ [m]
var e_copper_diam = 0.0; //____ [m]
var e_cableA = 0.0 ; //________ [m2] copper wire cross section

var e_copper_rho20 = 1.78e-8; //Ωm at 20 deg C
var e_copper_alpha = 0.0039; // 1/K
var e_copper_T = 30.0; //______ degC //  set fix
var e_copper_rho = 1.85e-8; //_ calc later again

// add model variables, preset to old anode, more see function modeldata()
var e_model = 0; //______________ default old anode
var e_Model_V = 2.1;  //_______ [V]
var e_Model_Ohm = 0.2 //_______ [Ω]

var e_biorock_V = 0.0; //______ [V]
var e_biorock_W = 0.0; //______ [W]
var e_cableR = 1.0 //__________ [ohm]
var e_cableV = 0.0; //_________ [V]
var e_cableW = 0.0; //_________ [W] losses
var e_PS_A_sec = 0.0; //_______ [A] calculated total Ampere per tunnel
var e_PS_A_overwrite = false;// true;
var e_PS_V_sec = 0.0; //_______ [V]
var e_PS_eff = 85; //__________ [%] // set fix
var e_PS_W_sec = 0.0; //_______ [W] out
var e_PS_V = 230.0; //_________ [V] in
var e_PS_A = 0.0; //___________ [A] in
var e_PS_W = 0.0; //___________ [W] in
var e_PS_Wa = 0.0; //__________ [W] per year
var e_PS_total_eff=0.0;//______ [%] total ratio / efficiency BIOROCK power to GRID power

// following calc variables are NOT used inside calc_list(), and not shown in spreadsheet,
// the math is done inside the documentation docu.js function bolps() 

var bolps_enable = true; //false;//true; //_____________________________ toggle key [b]
var GRID_BOLPS = false; //______________________________________________ only true for PROJECT 3 selector and in BOLPS GRAPH AC DC switch
 
var bolpsds='';
var e_BOLPS_diam = 0.00357; //_ [m] // set fix BOLPS cable diameter // change only in GRID BOLPS project 3
var e_BOLPS_DC_cathode_cable=5.0; //___ [m] DC cathode cable length
var e_BOLPS_DC_cable=0.0; //___ [m] cable length
var e_BOLPS_cableA=10.0; //____ [m2] wire cross section
var e_BOLPS_cableR=1.0; //_____ [ohm]
var e_BOLPS_cableV=0.0; //_____ [V] volt cable loss
var e_BOLPS_PS_W=0.0; //_______ [W] power input to BOLPS
var e_BOLPS_PS_V=0.0; //_______ [V] volt BOLPS output
var e_BOLPS_PS_inV=0.0; //_____ [v] volt BOLPS in
var e_BOLPS_eff=0.95; //_______ [%] // set fix

var e_BOLPSSELV_W=0.0; //______ [W] in
var e_BOLPSSELV_Wa=0.0; //_____ [W] in per year
var e_BOLPSSELV_V=230.0; //____ [V] in   //set fix
var e_BOLPSSELV_A=0.0; //______ [A] in
var e_BOLPSSELV_eff=0.90; //___ [%]      //set fix
var e_BOLPSSELV_sec_W=0.0; //__ [W] out
var e_BOLPSSELV_sec_V=48.0; //_ [V] out  //set fix
var e_BOLPSSELV_sec_A=0.0; //__ [A] out in 48V loop

var e_BOLPSSELV_cableR=0.0; //_ [ohm] cable in the 48V loop ( beach cable ) different because no anode cable now
var e_BOLPSSELV_cableV=0.0; //_ [V] volt over cable R between SELV and BOLPS

var e_BOLPSSELV_total_eff=0.0;//[%] total ratio / efficiency BIOROCK power to GRID power

//______________________________________________________________________ rev allow costs estimate in docu.js after adjust here:
//var euro_kWh = 0.22; //______ [€] per kWh in spain 2019 ?
var euro_kWh = 0.15; //________ [€] per kWh in spain 2020 ( Aitor email incl 21% VAT )
//var euro_steel = 0.40; //____ [€] per kg rebar steel delivered at site?
var euro_steel = 3.65; //______ [€] per kg rebar steel, welded by provider and delivered at site ( Aitor email )( KLL add incl 21% VAT )

var euro_cable = 0.269; //_____ [€] per meter copper sea cable per 1 mm2 copper wire crosssection use spain data from 13.3.2021 DN-F 0,6/1 kV 
var GRID_euro, steel_euro, cable_euro; // [€]
var GRID_BOLPS_euro, cable_BOLPS_euro,cable_BOLPSSELV_euro; // [€]

//______________________________________________________________________ markdown script see index.html
//https://github.com/jonschlinkert/remarkable
var md = new Remarkable();
md.set({
    html: false,
    typographer: true,
    breaks: true
});

var proj = 0;

//______________________________________________________________________
function projdata(sel) { //_____________________________________________ called from setup and myProjectSelectEvent() in DOM_code.js
 if ( sel == 0 ) { //___________________________________________________ 0 project default
   GRID_BOLPS = false;
   roddiam = 0.010;
   inp00.value(str(roddiam));
   arcN = 10;
   inp10.value(str(arcN));
   arc_dist = 0.445;
   inp11.value(str(arc_dist));   
   archeight = 2.0;
   inp13.value(str(archeight));   
   arcbase = 8.50;
   inp14.value(str(arcbase));   
   e_bamp = 2.0;
   inp20.value(str(e_bamp));   
   e_copper_diam = 0.00276;
   inp24.value(str(e_copper_diam));
   e_sel.selected('2.76 mm /  6 IEC'); //_______________________________ also set at dropdown
   p_PS_beach = 20.0;
   inp30.value(str(p_PS_beach));
   p_beach_struct = 40.0;
   inp31.value(str(p_beach_struct));
   p_struct_anode = 2.0;
   inp32.value(str(p_struct_anode));
   p_struct_depth = 3.0;
   inp33.value(str(p_struct_depth));
   bolps_enable = true; //false;
   e_BOLPSSELV_sec_V = 48.0; //__________________________________ NEW OVERWRITE
 }
 if ( sel == 1 ) { //___________________________________________________ 1 project small
   GRID_BOLPS = false;
   roddiam = 0.008;
   inp00.value(str(roddiam));
   arcN = 10;
   inp10.value(str(arcN));
   arc_dist = 0.445;
   inp11.value(str(arc_dist));   
   archeight = 1.0;
   inp13.value(str(archeight));   
   arcbase = 8.50;
   inp14.value(str(arcbase));   
   e_bamp = 2.0;
   inp20.value(str(e_bamp));   
   e_copper_diam = 0.00226;
   inp24.value(str(e_copper_diam));
   e_sel.selected('2.26 mm /  4 IEC'); //_______________________________ also set at dropdown
   p_PS_beach = 20.0;
   inp30.value(str(p_PS_beach));
   p_beach_struct = 40.0;
   inp31.value(str(p_beach_struct));
   p_struct_anode = 2.0;
   inp32.value(str(p_struct_anode));
   p_struct_depth = 3.0;
   inp33.value(str(p_struct_depth));
   bolps_enable = false;
   e_BOLPSSELV_sec_V = 48.0; //__________________________________ NEW OVERWRITE
 }
 if ( sel == 2 ) { //___________________________________________________ 2 project big and BOLPS
   GRID_BOLPS = false;
   e_BOLPSSELV_sec_V = 48.0; //__________________________________ NEW OVERWRITE
   roddiam = 0.012;
   inp00.value(str(roddiam));
   arcN = 10;
   inp10.value(str(arcN));
   arc_dist = 0.445;
   inp11.value(str(arc_dist));   
   archeight = 3.0;
   inp13.value(str(archeight));   
   arcbase = 8.50;
   inp14.value(str(arcbase));   
   e_bamp = 2.0;
   inp20.value(str(e_bamp));   
   e_copper_diam = 0.00226;
   inp24.value(str(e_copper_diam));
   e_sel.selected('2.26 mm /  4 IEC'); //_______________________________ also set at dropdown
   p_PS_beach = 20.0;
   inp30.value(str(p_PS_beach));
   p_beach_struct = 40.0;
   inp31.value(str(p_beach_struct));
   p_struct_anode = 2.0;
   inp32.value(str(p_struct_anode));
   p_struct_depth = 3.0;
   inp33.value(str(p_struct_depth));
   bolps_enable = true;
 }
 // rework for true GRID_BOLPS 1.4.2021
 if ( sel == 3 ) { //___________________________________________________ 3 project big NO BOLPS calc / but GRID BOLPS simulation / 500m steel
   GRID_BOLPS = true;
   roddiam = 0.009;
   inp00.value(str(roddiam));
   arcN = 22;
   inp10.value(str(arcN));
   arc_dist = 0.19;
   inp11.value(str(arc_dist));   
   archeight = 3.0;
   inp13.value(str(archeight));   
   arcbase = 9.00;
   inp14.value(str(arcbase));   
   e_bamp = 2.0;
   inp20.value(str(e_bamp));   
   e_copper_diam = 0.00276; //__________________________________________ set default AC cable to 6 IEC 22.4.2021
   inp24.value(str(e_copper_diam));
   e_sel.selected('2.76 mm /  6 IEC'); //_______________________________ also set at dropdown ( AC CABLE )
   p_PS_beach = 20.0; // 
   inp30.value(str(p_PS_beach));
   p_beach_struct = 165.0; //___________________________________________ if we think in the 3BOLPSbox between 3 structures
   inp31.value(str(p_beach_struct));
   p_struct_anode = 6.0; //_____________________________________________ close to BOLPSbox
   inp32.value(str(p_struct_anode));
   p_struct_depth = 3.0;
   inp33.value(str(p_struct_depth));
   bolps_enable = true; //false;
   show_land = true; //false;
   e_BOLPSSELV_sec_V = 230; //48.0; //__________________________________ NEW OVERWRITE only here for GRID BOLPS project 3 used
   e_BOLPSSELV_eff = 1.0; //____________________________________________ just a 230VAC feeder
   e_BOLPS_eff=0.95; //_________________________________________________ [%] // set fix for UHP-1500W-24V
   e_BOLPS_diam = 0.00564; //_ [m] _____________________________________ NEW OVERWRITE 25mm2 IEC 16m DC cable
   e_BOLPS_DC_cathode_cable = 10; //____________________________________ NEW OVERWRITE from 5, anode cable length by user input
   //inp06.value(show_land); //___________________________________________ how to get this showing?
   //inp06.checked = false;
 }
 
}
//______________________________________________________________________

//______________________________________________________________________
function modeldata(sel) { //_____________________________________________ called from setup and myModelSelectEvent() in DOM_code.js
 if ( sel == 0 ) { //___________________________________________________ 0 model old anode mesh 
   e_Model_V = 2.1;  //_______ [V]
   e_Model_Ohm = 0.2 //_______ [Ω]
 }
 if ( sel == 1 ) { //___________________________________________________ 1 model new anode ribbon ( in PVC pipe and bad ground mounting )
   e_Model_V = 1.75; //_______ [V]
   e_Model_Ohm = 0.315; //____ [Ω]
 }
 if ( sel == 2 ) { //___________________________________________________ 2 questionable model ICCP anode ( if realy that good i will say sorry ! )
   e_Model_V = 0.00; //_______ [V]
   e_Model_Ohm = 0.216; //____ [Ω]
 }
 if ( sel == 3 ) { //___________________________________________________ 2 questionable model ELSYCA
   e_Model_V = 0.00; //_______ [V]
   e_Model_Ohm = 0.320; //____ [Ω]
 }
}
//______________________________________________________________________
function draw_object() { //_____________________________________________ called by / from inside PTZ
  //axis(); //__________________________________________________________ test graph
  push();
  if ( tunnelswitch ) rotateY(-PI/2); //________________________________ turn tunnel to parallel beach
  noFill();
  stroke('white');
  sphere(0.1); //_______________________________________________________ info center point
  strokeWeight(roddiam * 100);
  for (let i = 0; i < arcN; i++) {
    push()
    translate(0, 0, -i * arc_dist);
    if (i % 3 == 0) {
      line(-arcbase/2, 0, 0, arcbase/2, 0, 0); //_______________________ on ever 3d. arc baseline
      if ( show_steel ) { //____________________________________________ here need more steel for arc support, start with steel from arc base up to arc ever 3rd gridpoint
        // all we know is the array of arc weld points, so it not makes sense to make steel from arcbase up to any other point on arc / also for mechanical reasons.
        // longsN is ODD number, the top is then at
        let topN =  int(longsN/2.0);
        for ( let n = 0; n < topN; n+=3 ) {
          let wpv0 = awps[topN + i * longsN + n]; //____________________ get single vectors again /  actually only first arc data would suffice
          line(wpv0.x,wpv0.y,0,wpv0.x,0,0); //__________________________ not use wpv0.z as above already do translate about arcdist
          if ( n > 0 ) { //_____________________________________________ if not middle do it again to the right
            let wpv0 = awps[topN + i * longsN - n]; //__________________
            line(wpv0.x,wpv0.y,0,wpv0.x,0,0); //________________________
          }
        }
      }
    }
    pop();
  }
  if ( show_steel ) {
    for ( let s = 0; s < awps.length-1; s++ ) {
      let wpv0 = awps[s]; //____________________________________________ get single vectors again
      let wpv1 = awps[s+1]; //__________________________________________ get next single vector
      if ( s % longsN != longsN-1 ) 
        line(wpv0.x,wpv0.y,wpv0.z,wpv1.x,wpv1.y,wpv1.z); //_____________ show arcs as lines
    }
    for ( let x = 0; x < longsN; x++ ) { //_____________________________ show longs
      let wpv0 = awps[x]; //____________________________________________ get fIrst arc points
      let wpv1 = awps[awps.length-longsN+x]; //_________________________ get last arc points 
      line(wpv0.x,wpv0.y,wpv0.z,wpv1.x,wpv1.y,wpv1.z); //_______________ show longs
    }
  } else {
    for ( let s = 0; s < awps.length; s++ ) {  //_______________________ draw WELD POINTS ( not the connecting steel ) from array of vectors
      let wpv0 = awps[s]; //____________________________________________ get single vectors again
      push();
      translate(wpv0.x,wpv0.y,wpv0.z);
      sphere(roddiam); //_______________________________________________ first step only show arc weld points ( no steel lines... )
      pop();
    }    
  }
  pop();
  //____________________________________________________________________ draw beach power supply
  push();
  translate(0,-0.75,p_beach_struct+p_PS_beach);
  stroke(0,200,0);
  strokeWeight(0.2);
  fill(200,0,200);
  box(1,1.5,0.4);
  pop();
  //____________________________________________________________________ draw far anode
  push();
  translate(0,-0.3,-p_struct_anode);
  rotateZ(-PI/2); //____________________________________________________ cylinder flat on ground
  noStroke();
  fill(200,0,0);
  cylinder(0.3,1.2);
  pop();
  //____________________________________________________________________ draw power cable
  stroke('red');
  strokeWeight(1.5)
  line(-0.3,0,-p_struct_anode,-0.3,0,p_beach_struct+p_PS_beach);
  stroke('white'); //___________________________________________________ same color as accreted steel cathode ( show same potential )
  strokeWeight(1.5)
  line(0,0,0,0,0,p_beach_struct+p_PS_beach);
  
  if (show_land) {
    noStroke();
    push();
    rotateX(-PI/2);
    fill(180,180,0,100); //_____ _______________________________________ yellow sand ground
    rect(-500,-500,1000,1000);
    pop();
    push();
    translate(0,0,p_beach_struct);
    rotateX(-PI/2-p_slope); //__________________________________________ if you think water is horizontal but is not here! we use ground horizontal
    fill(0,0,180,100); //_______________________________________________ blue water level
    rect(-500,0,1000,500+p_beach_struct);
    pop();
  }
}

function setup() {
  cnv = createCanvas(800, 500, WEBGL);
  //cnv.position(50,200);
  cnv.parent('canvaspos');
  create_DOM(); //______________________________________________________ see file DOM_code.js
  if ( proj >= 0 ) projdata(proj); //___________________________________ set the selected project setpoints ( see sketch_biorock.js )
  calc_list(); //_______________________________________________________ below function for data calc and documentation 
}

function draw() {
  background(0, 0, 80);
  PTZ();
}

function calc_list() { //_______________________________________________ recalc at setup and after change of settings
  // SITE
  let p_waterline = sqrt( sq(p_beach_struct) - sq(p_struct_depth) ); //_
  p_slope = acos(p_waterline/p_beach_struct); //________________________ beach slope ( like after structure depth or structure to beach changed )
  // TUNNEL
  tunnellength = (arcN -1) * arc_dist; //_______________________________ calc length of tunnel
  inp12.value(str(tunnellength)); //____________________________________ show it

  //____________________________________________________________________ http://mathcentral.uregina.ca/QQ/database/QQ.09.07/s/bruce1.html
  arcrad = archeight/2.0 + arcbase*arcbase/(archeight*8.0);
  //____________________________________________________________________ https://www.mathopenref.com/arcradius.html
  X = arcrad-archeight;
  arcPHI = acos(X/arcrad) * 2.0;
  arclength = arcrad * arcPHI; //_______________________________________ that looks very tricky??
  inp15.value(str(arclength)); //_______________________________________ show it

  rodm = arcN * arclength; //___________________________________________ calc arc length METER

  longsN = int(arclength / arc_dist) + 1; //____________________________ number of top steel ( to form a rectangular grid on/as tunnel)
  if ( longsN % 2 == 0 ) longsN++; //___________________________________ only on odd longsN will have one on the TOP / middle
  
  calc_weldpointsarray(); //____________________________________________ after we know longsN, we can calculate the arc structure
  
  rodm +=  tunnellength * longsN; //____________________________________ add top grid meter

  arcbaseN=0;
  for (let i = 0; i < arcN; i++)
    if (i % 3 == 0) {
      arcbaseN++;
      rodm += arcbase; //_______________________________________________ add groundlines at every 3rd arc ?
      
      let topN =  int(longsN/2.0);
      for ( let n = 0; n < topN; n+=3 ) {
        let wpv0 = awps[topN + i * longsN + n]; //______________________ get single vectors again /  actually only first arc data would suffice
        //console.log('i ',i,' n ',n,' y ',wpv0.y);
        rodm += wpv0.y*-1.0; //_________________________________________ -1 * .y is the steel length from ground / arcbase
        if ( n > 0 ) { //_______________________________________________ if not middle do it again to the right
          let wpv0 = awps[topN + i * longsN - n]; //____________________
          //console.log('i ',i,' n (-)',n,' y ',wpv0.y);
          rodm += wpv0.y*-1.0;
        }
      }
    }
  inp01.value(str(rodm)); //____________________________________________ show it

  rodA = rodm * roddiam * PI; //________________________________________ calc steel surface [m2]
  inp02.value(str(rodA)); //____________________________________________ show it

  rodV = PI * roddiam * roddiam * rodm/4.0; //__________________________ calc steel volume PI * (d/2)**2 * L [m3]
  inp03.value(str(rodV)); //____________________________________________ show it

  rodM = rodV * specweight; //__________________________________________ calc steel weight [kg]
  inp04.value(str(rodM)); //____________________________________________ show it
  //____________________________________________________________________ print to CONSOLE or HTML or DOWNLOAD FILE
  docu(); //____________________________________________________________ see docu.js see there now all electric calculations are included there too.
}

function calc_weldpointsarray() {  //___________________________________ needs: arcN, longsN, arcrad, arcPHI, arc dist, archeight
  // make a array of arc weld points awps[] of vectors wpv[x,y,z] so we not need to do it at every draw
  awps = []; //_________________________________________________________ reset
  for (let i = 0; i < arcN; i++) { //___________________________________ loop over all arcs
    let sd = round(longsN/2.0);
    for ( let s = -sd+1; s < sd; s++ ) { //_____________________________ loop over angle steps of one arc
      let PHI = s*arcPHI/(longsN-1);
      let xarc = arcrad * sin(PHI);
      let yarc = arcrad * cos(PHI);
      wpv = createVector(xarc,-yarc +arcrad -archeight,-i*arc_dist);
      awps.push(wpv); //________________________________________________ store to array
    }
  }  
}

