// DOM_code.js
var slT, slX, slY, slZ; // optional PTZ slider operation elements
var inT, inT07; //  
var inp00, but00, inp01, but01, inp02, but02, inp03, but03, inp04, but04,               inp06, inp07; 
var inp10, but10, inp11, but11, inp12, but12, inp13, but13, inp14, but14, inp15, but15, inp16, inp17;
var inp20, but20, inp21, but21, inp22, but22, inp23, but23, inp24, but24, inp25, but25, inp26, inp27;
var inp30, but30, inp31, but31, inp32, but32, inp33, but33, inp34, but34, inp35, but35;
var but00t,but01t,but02t,but03t,but04t;
var but10t,but11t,but12t,but13t,but14t,but15t;
var but20t,but21t,but22t,but23t,but24t,but25t;
var but30t,but31t,but32t,but33t,but34t,but35t;

let link_list ='https://blueregeneration.com/\nhttp://www.biorock.org/\nhttp://kll.engineering-news.org\nhttps://upload.wikimedia.org/wikipedia/commons/3/32/Comparison_wire_gauge_sizes.svg\nhttps://en.wikipedia.org/wiki/Extra-low_voltage \nhttps://docs.google.com/spreadsheets/d/1hgasQUpGNY2VDT27Jdus3ygX51Gic_MpCwbkact9KCI/edit?usp=sharing\nhttps://github.com/jonschlinkert/remarkable'
// create a DOM selector of a list of cable types, mixed AWG and IEC
var e_sel, p_sel;

var docp; // see DOM_code.js

function create_DOM() { //_________________ spread sheet like parameter input field
  //___________________________________________________________________________
  // in sketch_biorock.js there is the variable e_public preset
  // if false the electric design is not shown ( and here the corresponding DOM elements not created )
  // still i want try here now if anyhow it could be re-enabled by a html URL parameter like
  // biorock.html?e_public=1
  // or use a more complicated number/string like password 
  let e_public_pw = '1';
  let diagu = false;//true; // enable diagnostic print for URL analysis
  let urlp=[];
  let s=location.toString().split('?');
  if (diagu) console.log('s split ?',s);
  if ( s.length > 1 ) {
    s=s[1].split('&');
    if (diagu) console.log('s split &',s);
    for(i=0;i<s.length;i++){
      u=s[i].split('=');
      if (diagu) console.log('u split = ',u);
      urlp[u[0]]=u[1];
    }
  }
  if ( urlp['e_public'] == e_public_pw ) { 
    e_public = true; //________________________ re-enable if set false in sketch_biorock.js
    console.log('e_public reset by URL ',e_public);
  }
  //___________________________________________________________________________
  //console.log(platform); // use platform.min.js
  console.log('browser: ' + platform.name + ' v ' + platform.version + ' on ' + platform.os);
  //___________________________________________________________________________

  // spread sheet layout
  let yOFF = 150 + 500 + 10; // header + canvas + space 
  let xgrid = 190;
  let dxgrid = 140;
  let ygrid = 25;

  let gridcol = 0;
  let gridline = 0;

  if ( tablet ) { //_______________________ make PTZ additional sliders
    gridcol = 0;
    gridline = 0;
//    slT = createElement('u', ' PTZ slider:');
    slT = createElement('u', ' Rotate Tilt Zoom sliders:');
    slT.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
    //_____________________________________ slX
    gridcol = 1;
    gridline = 0;
    slX = createSlider(-180, 180, 0);
    slX.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
    slX.style('width', '170px');
    //_____________________________________ slY
    gridcol = 2;
    gridline = 0;
    slY = createSlider(-180, 180, 0);
    slY.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
    slY.style('width', '170px');
    //_____________________________________ slZ
    gridcol = 3;
    gridline = 0;
    slZ = createSlider(0.01, 60.0, 10.0,0.2);
    slZ.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
    slZ.style('width', '170px');
    
    yOFF += 75; //_________________________ move data grid down
  } else yOFF += 10;

  //move up under the slider / or canvas
  //_______________________________________ view options 
  gridcol = 0;
  gridline = -2;//6;
  inT = createElement('u', ' view options:');
  inT.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  
  //_______________________________________ show_land
  gridcol = 1;
  gridline = -2;//6;
  inp06 = createCheckbox('show land [s]', show_land);
  inp06.changed(myInputshow_land);
  inp06.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ show_steel
  gridcol = 2;
  gridline = -2;//6;
  inp16 = createCheckbox('show steel [a]', show_steel);
  inp16.changed(myInputshow_steel);
  inp16.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  //_______________________________________ tunnelswitch
  gridcol = 3;
  gridline = -2;//6;
  inp26 = createCheckbox('tunnel orientation [o]', tunnelswitch);
  inp26.changed(myInputtunnelswitch);
  inp26.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  //_______________________________________ text options
  gridcol = 0;
  gridline = -1;//6;
  inT07 = createElement('u', ' text options:');
  inT07.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  
  //_______________________________________ show help
  gridcol = 1;
  gridline = -1;//6;
  inp07 = createCheckbox('show help [h]', help);
  inp07.changed(myInputshow_help);
  inp07.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ show bolps
  gridcol = 2;
  gridline = -1;//6;
  inp17 = createCheckbox('show BOLPS [b]', bolps_enable);
  inp17.changed(myInputshow_bolps);
  inp17.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  //_______________________________________ download print
  gridcol = 3;
  gridline = -1;//6;
  inp27 = createCheckbox('download [d]', onedl);
  inp27.changed(myInputdownload);
  inp27.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //____________________________________________________________________ SPREAD SHEET
  //_______________________________________ roddiam
  gridcol = 0;
  gridline = 0;
  inp00 = createInput(str(roddiam));
  inp00.changed(myInputroddiam);
  inp00.style('font-size', '13px');
  inp00.style('color', '#0000ff');
  inp00.style('background-color', '#00ff00');
  inp00.size(40);
  inp00.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but00t = 'rod_diam__[m]';
  but00 = createButton(but00t); // 12char length
  but00.style('font-size', '13px');
  but00.style('color', '#0000ff');
  but00.style('background-color', '#aaaaaa');
  but00.size(dxgrid-20);
  but00.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ rodm SHOW
  gridcol = 0;
  gridline = 1;
  inp01 = createInput(str(rodm));

  inp01.style('font-size', '13px');
  inp01.style('color', '#0000ff');
  inp01.style('background-color', '#aaaaaa');
  inp01.size(40);
  inp01.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but01t = 'rod_length_[m]';
  but01 = createButton(but01t); // 12char length
  but01.style('font-size', '13px');
  but01.style('color', '#0000ff');
  but01.style('background-color', '#aaaaaa');
  but01.size(dxgrid-20);
  but01.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ rodA SHOW
  gridcol = 0;
  gridline = 2;
  inp02 = createInput(str(rodA));

  inp02.style('font-size', '13px');
  inp02.style('color', '#0000ff');
  inp02.style('background-color', '#aaaaaa');
  inp02.size(40);
  inp02.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but02t = 'surface___[m2]';
  but02 = createButton(but02t); // 12char length
  but02.style('font-size', '13px');
  but02.style('color', '#0000ff');
  but02.style('background-color', '#aaaaaa');
  but02.size(dxgrid-20);
  but02.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ rodV SHOW
  gridcol = 0;
  gridline = 3;
  inp03 = createInput(str(rodV));

  inp03.style('font-size', '13px');
  inp03.style('color', '#0000ff');
  inp03.style('background-color', '#aaaaaa');
  inp03.size(40);
  inp03.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but03t = 'volume___[m3]';
  but03 = createButton(but03t); // 12char length
  but03.style('font-size', '13px');
  but03.style('color', '#0000ff');
  but03.style('background-color', '#aaaaaa');
  but03.size(dxgrid-20);
  but03.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ rodM SHOW
  gridcol = 0;
  gridline = 4;
  inp04 = createInput(str(rodM));

  inp04.style('font-size', '13px');
  inp04.style('color', '#0000ff');
  inp04.style('background-color', '#aaaaaa');
  inp04.size(40);
  inp04.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but04t = 'weight____[kg]';
  but04 = createButton(but04t); // 12char length
  but04.style('font-size', '13px');
  but04.style('color', '#0000ff');
  but04.style('background-color', '#aaaaaa');
  but04.size(dxgrid-20);
  but04.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ arcN
  gridcol = 1;
  gridline = 0;
  inp10 = createInput(str(arcN));
  inp10.changed(myInputarcN);
  inp10.style('font-size', '13px');
  inp10.style('color', '#0000ff');
  inp10.style('background-color', '#00ff00');
  inp10.size(40);
  inp10.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but10t = 'arc count___#';
  but10 = createButton(but10t); // 12char length
  but10.style('font-size', '13px');
  but10.style('color', '#0000ff');
  but10.style('background-color', '#aaaaaa');
  but10.size(dxgrid-20);
  but10.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ arc_dist
  gridcol = 1;
  gridline = 1;
  inp11 = createInput(str(arc_dist));
  inp11.changed(myInputarc_dist);
  inp11.style('font-size', '13px');
  inp11.style('color', '#0000ff');
  inp11.style('background-color', '#00ff00');
  inp11.size(40);
  inp11.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but11t = 'arc_dist___[m]';
  but11 = createButton(but11t); // 12char length
  but11.style('font-size', '13px');
  but11.style('color', '#0000ff');
  but11.style('background-color', '#aaaaaa');
  but11.size(dxgrid-20);
  but11.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ tunnellength
  gridcol = 1;
  gridline = 2;
  inp12 = createInput(str(tunnellength));
  //inp12.changed(myInputtunnellength);
  inp12.style('font-size', '13px');
  inp12.style('color', '#0000ff');
  inp12.style('background-color', '#aaaaaa');
  inp12.size(40);
  inp12.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but12t = 'tunnellength[m]';
  but12 = createButton(but12t); // 12char length
  but12.style('font-size', '13px');
  but12.style('color', '#0000ff');
  but12.style('background-color', '#aaaaaa');
  but12.size(dxgrid-20);
  but12.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ archeight
  gridcol = 1;
  gridline = 3;
  inp13 = createInput(str(archeight));
  inp13.changed(myInputarcheight);
  inp13.style('font-size', '13px');
  inp13.style('color', '#0000ff');
  inp13.style('background-color', '#00ff00');
  inp13.size(40);
  inp13.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but13t = 'archeight__[m]';
  but13 = createButton(but13t); // 12char length
  but13.style('font-size', '13px');
  but13.style('color', '#0000ff');
  but13.style('background-color', '#aaaaaa');
  but13.size(dxgrid-20);
  but13.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ arcbase
  gridcol = 1;
  gridline = 4;
  inp14 = createInput(str(arcbase));
  inp14.changed(myInputarcbase);
  inp14.style('font-size', '13px');
  inp14.style('color', '#0000ff');
  inp14.style('background-color', '#00ff00');
  inp14.size(40);
  inp14.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but14t = 'tunnelwide_[m]';
  but14 = createButton(but14t); // 12char length
  but14.style('font-size', '13px');
  but14.style('color', '#0000ff');
  but14.style('background-color', '#aaaaaa');
  but14.size(dxgrid-20);
  but14.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ arclength
  gridcol = 1;
  gridline = 5;
  inp15 = createInput(str(arclength));
  //inp15.changed(myInputarclength);
  inp15.style('font-size', '13px');
  inp15.style('color', '#0000ff');
  inp15.style('background-color', '#aaaaaa');
  inp15.size(40);
  inp15.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but15t = 'arclength__[m]';
  but15 = createButton(but15t); // 12char length
  but15.style('font-size', '13px');
  but15.style('color', '#0000ff');
  but15.style('background-color', '#aaaaaa');
  but15.size(dxgrid-20);
  but15.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

if (e_public) {
  //_______________________________________ e_bamp
  gridcol = 2;
  gridline = 0;
  inp20 = createInput(str(e_bamp));
  inp20.changed(myInpute_bamp);
  inp20.style('font-size', '13px');
  inp20.style('color', '#0000ff');
  inp20.style('background-color', '#00ff00');
  inp20.size(40);
  inp20.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but20t = 'bamp_[A/m2]';
  but20 = createButton(but20t); // 12char length
  but20.style('font-size', '13px');
  but20.style('color', '#0000ff');
  but20.style('background-color', '#aaaaaa');
  but20.size(dxgrid-20);
  but20.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ e_PS_A  ( SHOW or MANUAL OVERWRITE )
  gridcol = 2;
  gridline = 1;
  inp21 = createInput(str(e_PS_A_sec));
  inp21.changed(myInpute_PS_A); // allow manual override to test pure eleectric calc without steel A check
  inp21.style('font-size', '13px');
  inp21.style('color', '#0000ff');
  inp21.style('background-color', '#00af00');
  inp21.size(40);
  inp21.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but21t = 'ampere__[A]';
  but21 = createButton(but21t); // 12char length
  but21.style('font-size', '13px');
  but21.style('color', '#0000ff');
  but21.style('background-color', '#aaaaaa');
  but21.size(dxgrid-20);
  but21.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ e_PS_V_sec SHOW
  gridcol = 2;
  gridline = 2;
  inp22 = createInput(str(e_PS_V_sec));
  inp22.style('font-size', '13px');
  inp22.style('color', '#0000ff');
  inp22.style('background-color', '#aaaaaa');
  inp22.size(40);
  inp22.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but22t = 'PS Volt_[V]';
  but22 = createButton(but22t); // 12char length
  but22.style('font-size', '13px');
  but22.style('color', '#0000ff');
  but22.style('background-color', '#aaaaaa');
  but22.size(dxgrid-20);
  but22.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ e_PS_W SHOW
  gridcol = 2;
  gridline = 3;
  inp23 = createInput(str(e_PS_W));
  inp23.style('font-size', '13px');
  inp23.style('color', '#0000ff');
  inp23.style('background-color', '#aaaaaa');
  inp23.size(40);
  inp23.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but23t = 'PS power_[W]';
  but23 = createButton(but23t); // 12char length
  but23.style('font-size', '13px');
  but23.style('color', '#0000ff');
  but23.style('background-color', '#aaaaaa');
  but23.size(dxgrid-20);
  but23.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ e_copper_diam
  gridcol = 2;
  gridline = 4;
  inp24 = createInput(str(e_copper_diam));
  inp24.changed(myInpute_copper_diam);
  inp24.style('font-size', '13px');
  inp24.style('color', '#0000ff');
  inp24.style('background-color', '#00ff00');
  inp24.size(40);
  inp24.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but24t = 'copper diam[m]';
  but24 = createButton(but24t); // 12char length
  but24.style('font-size', '13px');
  but24.style('color', '#0000ff');
  but24.style('background-color', '#aaaaaa');
  but24.size(dxgrid-20);
  but24.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  //_______________________________________ e_DC_cable SHOW
  gridcol = 2;
  gridline = 5;
  inp25 = createInput(str(e_DC_cable));
  inp25.style('font-size', '13px');
  inp25.style('color', '#0000ff');
  inp25.style('background-color', '#aaaaaa');
  inp25.size(40);
  inp25.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but25t = 'totalcable_[m]';
  but25 = createButton(but25t); // 12char length
  but25.style('font-size', '13px');
  but25.style('color', '#0000ff');
  but25.style('background-color', '#aaaaaa');
  but25.size(dxgrid-20);
  but25.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ e_cable SELECTOR moved 
  gridcol = 3; //2;
  gridline = 4; //6;
  setup_wire_diameter_select(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ BIOROCK model SELECTOR 
  gridcol = 2;
  gridline = 6;
  setup_model_select(10 + gridcol * xgrid, yOFF + gridline * ygrid);


  console.log(link_list);


}

  //_______________________________________ p_PS_beach
  gridcol = 3;
  gridline = 0;
  inp30 = createInput(str(p_PS_beach));
  inp30.changed(myInputp_PS_beach);
  inp30.style('font-size', '13px');
  inp30.style('color', '#0000ff');
  inp30.style('background-color', '#00ff00');
  inp30.size(40);
  inp30.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but30t = 'PS_beach__[m]';
  but30 = createButton(but30t); // 12char length
  but30.style('font-size', '13px');
  but30.style('color', '#0000ff');
  but30.style('background-color', '#aaaaaa');
  but30.size(dxgrid-20);
  but30.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ p_beach_struct
  gridcol = 3;
  gridline = 1;
  inp31 = createInput(str(p_beach_struct));
  inp31.changed(myInputp_beach_struct);
  inp31.style('font-size', '13px');
  inp31.style('color', '#0000ff');
  inp31.style('background-color', '#00ff00');
  inp31.size(40);
  inp31.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but31t = 'beach_struct[m]';
  but31 = createButton(but31t); // 12char length
  but31.style('font-size', '13px');
  but31.style('color', '#0000ff');
  but31.style('background-color', '#aaaaaa');
  but31.size(dxgrid-20);
  but31.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ p_struct_anode
  gridcol = 3;
  gridline = 2;
  inp32 = createInput(str(p_struct_anode));
  inp32.changed(myInputp_struct_anode);
  inp32.style('font-size', '13px');
  inp32.style('color', '#0000ff');
  inp32.style('background-color', '#00ff00');
  inp32.size(40);
  inp32.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but32t = 'struct_anode[m]';
  but32 = createButton(but32t); // 12char length
  but32.style('font-size', '13px');
  but32.style('color', '#0000ff');
  but32.style('background-color', '#aaaaaa');
  but32.size(dxgrid-20);
  but32.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ p_struct_depth
  gridcol = 3;
  gridline = 3;
  inp33 = createInput(str(p_struct_depth));
  inp33.changed(myInputp_struct_depth);
  inp33.style('font-size', '13px');
  inp33.style('color', '#0000ff');
  inp33.style('background-color', '#00ff00');
  inp33.size(40);
  inp33.position(dxgrid + gridcol * xgrid, yOFF + gridline * ygrid);
  but33t = 'struct_depth[m]';
  but33 = createButton(but33t); // 12char length
  but33.style('font-size', '13px');
  but33.style('color', '#0000ff');
  but33.style('background-color', '#aaaaaa');
  but33.size(dxgrid-20);
  but33.position(10 + gridcol * xgrid, yOFF + gridline * ygrid);

  //_______________________________________ project selector dropdown SELECTOR
  gridcol = 3;
  gridline = 6;
  setup_project_select(10 + gridcol * xgrid, yOFF + gridline * ygrid);
  
  //____________________________________________________________________ add documentation as a following text paragraph
  docp = createP('KLL engineering for BIOROCK');
  let docY = 860;
  if ( tablet ) docY += 50; // because of new android friendly PTZ sliders move docu paragraph down more
  docp.position(10,docY);
  docp.class('docuP');
  //docp.parent('docupos');

}

function myInputroddiam() {
  roddiam = float(this.value());
  calc_list();
  console.log('roddiam: ', roddiam);
}

function myInputshow_land() { // now checkbox
  show_land = ! show_land; // toggle
  console.log('show_land: ', show_land);
}

function myInputshow_steel() { // now checkbox
  show_steel = ! show_steel; // toggle
  console.log('show_steel: ', show_steel);
}

function myInputtunnelswitch() { // now checkbox
  tunnelswitch = ! tunnelswitch; // toggle
  console.log('tunnelswitch: ', tunnelswitch);
}

function myInputshow_help() { // now checkbox
  help = ! help; // toggle
  calc_list();
  console.log('help: ', help);
}

function myInputshow_bolps() { // now checkbox
  bolps_enable = ! bolps_enable; // toggle
  calc_list();
  console.log('bolps_enable: ', bolps_enable);
}

function myInputdownload() { // now checkbox
//  onedl = ! onedl; // toggle
  calc_list();
  saveStrings(dllist, 'BIOROCK_datasheet_'+justnow+'.txt');
  console.log('onedl: ', onedl);
}

function myInputarcN() {
  arcN = int(this.value());
  calc_list();
  console.log('arcN: ', arcN);
}

function myInputarc_dist() {
  arc_dist = float(this.value());
  calc_list();
  console.log('arc_dist: ', arc_dist);
}

function myInputarcheight() {
  archeight = float(this.value());
  calc_list();
  console.log('archeight: ', archeight);
}

function myInputarcbase() {
  arcbase = float(this.value());
  calc_list();
  console.log('arcbase: ', arcbase);
}

function myInpute_copper_diam() {
  e_copper_diam = float(this.value());
  calc_list();
  console.log('e_copper_diam: ', e_copper_diam);
}

function myInpute_bamp() {
  e_bamp = float(this.value());
  calc_list();
  console.log('e_bamp: ', e_bamp);
}

function myInpute_PS_A() { //________________________ allow manual overwrite
  e_PS_A_sec = float(this.value());
  e_PS_A_overwrite =  true; // NO RESET PROVIDED UP TO NOW
  inp21.style('background-color', '#ffaf00');
  calc_list();
  console.log('MANUAL OVERWRITE: e_PS_A_sec: ', e_PS_A_sec);
}

function myInputp_PS_beach() {
  p_PS_beach = float(this.value());
  calc_list();
  console.log('p_PS_beach: ', p_PS_beach);
}

function myInputp_beach_struct() {
  p_beach_struct = float(this.value());
  calc_list();
  console.log('p_beach_struct: ', p_beach_struct);
}

function myInputp_struct_anode() {
  p_struct_anode = float(this.value());
  calc_list();
  console.log('p_struct_anode: ', p_struct_anode);
}

function myInputp_struct_depth() {
  p_struct_depth = float(this.value());
  if ( p_struct_depth >= p_beach_struct ) {
    console.log('p_struct_depth can not be deeper as cable is long ', p_struct_depth);
    p_struct_depth = p_beach_struct-1; //_______________________________ limit wrong input ( it can not be deeper as cable is long )
    inp33.value(p_struct_depth); //_____________________________________ and write back
  }
  calc_list();
  console.log('p_struct_depth: ', p_struct_depth);
}

function setup_wire_diameter_select(sX,sY) { //______________________________ allow cable typ select from list
  e_sel = createSelect();
  e_sel.position(sX,sY);
//https://upload.wikimedia.org/wikipedia/commons/3/32/Comparison_wire_gauge_sizes.svg  
  e_sel.option('1.63 mm / 14 AWG');
  e_sel.option('1.78 mm / 2.5 IEC');
  e_sel.option('1.83 mm / 13 AWG');
  e_sel.option('2.05 mm / 12 AWG');
  e_sel.option('2.26 mm /  4 IEC');
  e_sel.option('2.30 mm / 11 AWG');
  e_sel.option('2.59 mm / 10 AWG');
  e_sel.option('2.76 mm /  6 IEC');
  e_sel.option('2.91 mm /  9 AWG');
  e_sel.option('3.26 mm /  8 AWG');
  e_sel.option('3.57 mm / 10 IEC');
  e_sel.option('3.66 mm /  7 AWG');
  e_sel.option('4.12 mm /  6 AWG');
  e_sel.option('4.51 mm / 16 IEC');
// 0.9.7.4c add more cable
  e_sel.option('4.62 mm /  5 AWG');
  e_sel.option('5.19 mm /  4 AWG');
  e_sel.option('5.64 mm / 25 IEC');  
  e_sel.option('5.83 mm /  3 AWG');
  e_sel.option('6.54 mm /  2 AWG');
  e_sel.option('6.68 mm / 35 IEC');    
  e_sel.option('7.35 mm /  1 AWG');
  e_sel.option('7.98 mm / 50 IEC');  
// 0.9.7.5 add more cable
  e_sel.option('9.44 mm / 70 IEC');  
  e_sel.option('11.0 mm / 95 IEC');  
  e_sel.option('11.7 mm /4/0 AWG');  
  e_sel.option('12.4 mm /120 IEC');  
  e_sel.option('13.1 mm /5/0 AWG');  
  e_sel.option('13.6 mm /150 IEC');  
    
  e_sel.selected('2.26 mm /  4 IEC'); //_______________________________ start same as diameter setting
  e_sel.changed(myCableSelectEvent);
}

function myCableSelectEvent() {
  let item = e_sel.value();
  let splitString = split(item, ' ');
  let set_m = float(splitString[0])/1000.0;
  e_copper_diam = set_m; //_____________________________________________ set as if we changed the diamater
  inp24.value(str(e_copper_diam)); //___________________________________ and write it back to its input field
  calc_list();
  console.log('you selected a: ' + item + '_ diameter cable, set_m: ' + set_m + ' [m]');
}

// 0.9.7 allow multiple presettings and selection from dropdown list

function setup_project_select(sX,sY) { //_______________________________ allow select of project settings from list
  p_sel = createSelect();
  p_sel.position(sX,sY);
  p_sel.option('0 project default');
  p_sel.option('1 project small');
  p_sel.option('2 project big and BOLPS');
  p_sel.option('3 GRID-BOLPS simulation');
  p_sel.selected('0 project default'); //_________________________ start default setting
  p_sel.changed(myProjectSelectEvent);
}

function myProjectSelectEvent() {
  let item = p_sel.value();
  let splitString = split(item, ' ');
  proj = int(splitString[0]);
  if ( proj >= 0 ) projdata(proj); //___________________________________ set the selected project setpoints ( see sketch_biorock.js )
  calc_list();
  console.log('you selected project ' + item );
}

// 0.9.7.4 allow slection of 2 BIOROCK MODEL tuning sets
function setup_model_select(sX,sY) { //_________________________________ allow select of BIOROCK MODEL settings from list
  m_sel = createSelect();
  m_sel.position(sX,sY);
  m_sel.option('0 model original');
  m_sel.option('1 anode ribbon test');
  m_sel.option('2 ICCP anode');
  m_sel.option('3 ELSYCA');
  m_sel.selected('0 model original'); //________________________________ start with default setting
  m_sel.changed(myModelSelectEvent);
}

function myModelSelectEvent() {
  let item = m_sel.value();
  let splitString = split(item, ' ');
  e_model = int(splitString[0]);
  if ( e_model >= 0 ) modeldata(e_model); //____________________________ set the selected model setpoints ( see sketch_biorock.js )
  calc_list();
  console.log('you selected biorock model ' + item );
}
