// docu.js // feed docu text and current variable to a string
// MOD  issues and print grid to download file
// MOD2 TPS english and formatting
// MOD3 markdown header ##
// MOD3b use markdown lib
// MOD4 more help text and optionbuttons instead keyoperation [h] [b] [d]

function print_DataGrid() {
  infod = ''; // clear
  infod += 'Data Grid: \n';
  infod += '  '+but00t+'  '+nf(roddiam,0,4)+'  '+but10t+'  '+nf(arcN,0,0)+'     '+but20t+'  '+nf(e_bamp,0,2)+'     '+but30t+'    '+nf(p_PS_beach,0,1)+'\n';
  infod += '  '+but01t+'  '+nf(rodm,0,1)+'  '+but11t+'  '+nf(arc_dist,0,2)+'  '+but21t+'  '+nf(e_PS_A_sec,0,2)+'    '+but31t+'  '+nf(p_beach_struct,0,1)+'\n';
  infod += '  '+but02t+'  '+nf(rodA,0,3)+'  '+but12t+' '+nf(tunnellength,0,2)+'  '+but22t+'  '+nf(e_PS_V_sec,0,2)+'    '+but32t+'  '+nf(p_struct_anode,0,1)+'\n';
  infod += '  '+but03t+'  '+nf(rodV,0,4)+'  '+but13t+'  '+nf(archeight,0,2)+'  '+but23t+'  '+nf(e_PS_W,0,2)+'  '+but33t+'  '+nf(p_struct_depth,0,1)+'\n';
  infod += '  '+but04t+'  '+nf(rodM,0,1)+'  '+but14t+'  '+nf(arcbase,0,2)+'  '+but24t+'  '+nf(e_copper_diam,0,5)+'  \n';
  infod += '                         '+but15t+'  '+nf(arclength,0,2)+'  '+but25t+'  '+nf(e_DC_cable,0,2)+' \n';
  infod += '\n';  
}

function docu() { 

  info = ''; // clear
  if ( help ) {
    info += '## ABOUT:\n';
    info += 'This application is an interactive tool prepared to demonstrate the \n';
    info += 'design of a single tunnel structure\n';
    if (e_public) info += 'and its electric power supply layout calculation\n';
    info += 'for a blueregeneration.com beach erosion protection project,\n';
    info += 'using BIOROCK(R) accretion concept Wolf HILBERTZ / Tom GOREAU patent: US5543034A \n';
    info += 'The tool serves as a basis for a Biorock electric engineer to quickly visualize \n';
    info += 'a project module and calculates steel, cable, voltage and current requirements,\n';
    info += 'and was not intended to be used by clients, artists or independent project designers,\n';     
    info += 'as it requires BIOROCK knowhow beyond cathodic protection and electrolysis.\n';
    info += '\n';
    info += '## OPERATION:\n';
    info += 'using Pan Tilt Zoom PTZ 3D view:\n';
    if ( usekey ) { // from PTZ.js
      info += 'key UP DOWN RIGHT LEFT -> rotate \nkey PAGE UP or [-], DOWN or [+] -> zoom\n';
    }
    info += ': by mouse:\n';
    info += 'LEFT press drag -> rotate \nRIGHT press -> move\nWHEEL turn -> zoom\n';
    //info += 'this mouse ( left or right ) first pressed is detected only inside the drawing canvas\nwhile the mouse move then still works also outside the canvas window\n';
    //info += 'even Tilde is adjusted to a 360 deg rotation X or Y acc canvas window size\n';
    info += ' \n: by 3 sliders:\n';
    info += 'Rotation, Tilt, Zoom, what allows operation also from android or other touch screens.\n';
    info += ' \n';
    info += ': VIEW OPTION by check boxes or key operation :\n';
    info += '[s] show Land\n';
    info += '[a] show Steel\n';
    info += '[o] Structure Orientation to beach\n';
    info += ' \n';
    info += 'Notes: our little world is flat \nmeans we see the ground as horizontal and the middle of the first tunnel arc as center.\n';
    info += 'The elements of a BIOROCK system:\n';
    info += '* the beach Power Supply,\n* low volt DC cables,\n* BIOROCK structure / tunnel / cathode\n* BIOROCK anode \n\n';
    info += 'are in one horizontal line on processing Z axis.\n';
    info += ' \n';
    info += 'if land view is enabled you see add to the ground plane a water surface plane,\nstart at beach in the angle '+nf(degrees(p_slope),0,1)+' deg up from ground,\n';
    info += 'given by your distance settings beach to structure '+nf(p_beach_struct,0,1)+' m & structure depth '+nf(p_struct_depth,0,1)+' m\n';  
    info += 'but with this wrong coordinate system, the beach slope is shown as water slope.\n';
    info += 'in the side view it is not possible to view the water level as horizontal. This is a known issue.\n';  
    info += ' \n';
    info += ': using SPREAD SHEET area:\n';    
    info += 'Design settings can be adjusted by entering decimal values into the GREEN cells.\n';
    info += 'The other cell values are automatically calculated and updated by the application,\nbased on / after GREEN cell user input.\n';
    info += ' \n';
    info += 'With the project drop down selector you can start from 3 predefined settings ( for the green cells ) \n';
    info += ' \n';
    info += ' \n: Exceptions:\n\n';
    info += 'note the dark-green color of the ampere __[A].\nThe value of this cell is automatically updated based on user input however,\n';
    info += 'this can be manually adjusted and useful while selecting power supply models.\n';
    info += 'If the user manually enters a value, the cell will display with an orange background color.\n';
    info += ' \n';
    info += 'copper diameter [m] allows the user to manually enter any non-standard cable/wire diameter value,\n';
    info += 'but right to it is a drop down selector of internationally used wire size (diameter)\n';
    info += 'to make this setting more easy.\n';
    info += ' \n';
    info += ': Steel - Rebar:\n';
    info += 'We assume "normal" construction steel commonly known as Rebar will be used to build structures.\nIn general, we use 0.009 or 0.012m diameter - termed 9 or 12mm rebar.\n';
    info += 'these are solid filled cylinders of steel with no hollow properties\nfrom which we can obtain a surface area value.\n';
    info += ' \nNote:\nRebar is available in several grades - ?common Grades are 33, 60, 90?. The grade of the material\ncorresponds to the amount of tensile strength it has in terms of ?pounds per square inch?.\nAdditional testing is needed to determine possible effects on electrical characteristics.\n';
    info += ' \n';
    info += ': regarding tunnel length:\nas the arc support is hard-coded every 3rd arc, for cell arc_count__# best use like 4/7/10/13/... only\n';  
    info += ' \n';
    info += ': using the detail text info area:\n';
    info += 'if HTML text paragraph is printed you see this info, plus the detail design parameter in text form \n';
    info += 'and a Bill Of Materials what can help with the production of this tunnel.\n';
    info += ' \n';
    info += 'this text is updated every time you change a parameter ( in the green cells )\nand if enabled, send as download to your browser.\n';
    info += ' \n';
    info += ': SAVE / PRINT options: \n';
    info += 'Project settings can be output ( saved locally ) via a downloaded text file\n';
    info += '[d] creates the download file of this text ( with current settings )\n';
    info += 'or through standard browser print functionality.\nPrinting via browser includes the 3D drawing, current settings and spreadsheet area.\n';
    info += 'what is the best way of project documentation:\n';
    info += '[ctrl/cmd][p] browser menu print ( like to PDF file ).\n';
    info += ' \n';
    info += ': PRIVACY NOTE:\nby this JavaScript tool no code is executed on \n/ and no data are send to / the server  this webpage comes from,\n';
    info += 'and the word download is insofar confusing, as the optional downloaded text file\nis generated in your PC RAM ( and not send from/to WWW )\nand saved, acc your browser settings, to your PC Drive.\n';
    info += ' \n';
    info += '## USE:\n';
    info += 'this calculation tool can provide estimated electric power requirements [V] and [A]\n';
    info += '( for a working point ) depending on: \n';
    info += '* BIOROCK current setpoint according cathode steel surface\n';
    info += '* BIOROCK process model \n';
    info += '* cable losses \n';
    info += ' \n';
    info += '### workflow:\n';
    info += '* 1- Site Layout\n';
    info += 'for the module need a power supply on the beach, please type in the distance of that PS to waterfront \n';
    info += 'add for the cathode cable need the distance to the structure\n';
    info += 'if the BIOROCK anode is far from structure need that distance setting too, negative values allowed\n';
    info += 'with this the length of cathode and anode cable will be calculated.. \n';
    info += 'add you can give the average depth at the structure position, used in the 3D show\n';
    info += ' \n';
    info += '* 2- Steel Data \n';
    info += 'select a steel type like 9mm or 12mm building steel.\n';
    info += ' \n';
    info += '* 3- design the tunnel by: \n';
    info += 'arc count and distance --> length of tunnel \n';
    info += 'arc height and width of tunnel --> arclength as a circle segment.\n';
    info += ' \n';
    info += '* 4- from this the steel surface is calculated and with the BIROCK process setting\n( possible 2A/m2 for best accretion )\n';
    info += 'the needed current setpoint [A].\n';    
    info += ' \n';
    info += '* 5- Electric cable selection\n';
    info += 'the length we got from the site layout already,\n';
    info += 'with the selection of a cable / wire copper diameter ( by type in or dropdown list )\n';
    info += 'the electric losses and power supply working point is calculated.\n';
    info += 'only a experienced electrician can interprete the results and select a cable size\n';
    info += 'what allows the use of a available PS and cable type,\nwith respect of the installation and running costs.\n';
    info += ' \n';
    info += '* 6- BIOROCK Model selection \n';
    info += 'currently we have 2 electric models for BIOROCK \n';
    info += 'OLD: with a anode type mesh \n';
    info += 'NEW: with a anode made of long ribbon material but folded to about 40cm in a PVC pipe with holes,\n  this one has a higher resistance. \n';
    info += 'in the selector you can choose which one you use, and find a different needed Voltage for the PS \n';
    info += ' \n';
    info += '* 7- BOLPS option \n';
    info += 'if the cable are long and the current is high, the overall efficiency is bad and we should check if\n';
    info += 'the BOLPS option could help, using 48VDC down to the structure and a power converter near structure\n';
    info += 'as a higher voltage will reduce the cable losses.\n';
    info += ' \n';
    info += '* 8- in case also this is not feasable you might need to redesign the tunnel\n';
    info += ' \n';
    info += ': boundaries : \n';
    info += 'this tool is about ONE anode and ONE steel structure\n';
    info += 'but theoretically that could be  distributed but electrically connected smaller structures.\n';
    info += '( think about you design a small tunnel, and then type in 4 times that current to power\n';
    info += '4 of this tunnels in form of a star ( or ring ) from a centered anode. )\n';
    info += 'we call this a MODULE, where in a PROJECT we could build multiple of this modules\n';
    info += 'but mechanically and electrically they may NOT be connected.\n';
    info += ' \n';
    info += 'if you want to think of a electrical GROUND point in a module or project\n';
    info += 'it would be the sea-bed the structures are standing on.\n';
    info += '* homework: you should try to calculate the related potential of the PS Minus point?\n';
    info += ' \n';
  } 

  // end operation info, add some first install user info.. but should disable later
  infos='';
  infos += '## SETUP:\n';
  infos += 'this project code is delivered as biorock.zip file.\n';
  infos += 'you can unzip it to a subdirectory and double click biorock.html\n';
  infos += 'as it is a pure html + javascript ( + processing P5.js ) project\nit will even run without server environment in your browser\n';
  infos += 'but it is supposed to be copied (uploaded / and called from ) a public online server.\n';
  infos += ' \n';
  infos += 'autodl, to send you the documentation ( THIS ) as a textfile =download= after change\n';
  infos += 'but if autodl false, still by \nkey [d] can get the download file manually ( with the date of last change )\n';
  infos += ' \n';
  infos +='there are 3 code toggle variables for this datasheet show:\n';
  infos +='* diagc = true; //__________ print calc info to browser console...\n';
  infos +='* diaghtml = true; //_______ print calc info to browser page paragraph...\n';
  infos +='* autodl = false; //________ print calc info to download file,\n\n';
  infos +='adjust that presettings in the code to your needs.\n';
  infos +=' \n';
  infos +='in sketch_biorock.js there is the variable \n* e_public / preset true\n';
  infos +='if false the electric design is not shown ( and the corresponding DOM elements not created )\n';
  infos +='still it can then be re-enabled by a html URL parameter like\n';
  infos +='/biorock.html?e_public=1\n';
  infos +='( or use a more complicated number/string like password )\n';
  infos +='this would make it possible to use this design tool for show / or project calculation\n';
  infos +='and use this to call it differently,\nfor example by a CMS ( like WordPress ) depending on the users login status...\n';
  infos +=' \nas the STEEL BOM text indicates this tool already creates a tunnel with support every 3rd arc,\nmore steel rods are required to withstand heavy waves..\nfor this code it means more drawing, more steel calc, more BOM list\n';
  infos +=' \n';
  infos +='as support for the project calculation,\n';
  infos +='now have the add calculation about steel / cable / electric consumption ( per year ) costs\n';
  infos +='make sure you adjust the settings: euro_kWh, euro_steel, euro_cable first!\n';
  infos +='where the last one requires most investigation,\na cable price per [m] cable length and [mm2] copper cross section, so actually proportional to m3 copper\n';
  infos +='what is a usual approach, but might not be so valid for sea cable?\n';
  infos +='where insulation and armour costs might be higher as copper price?\n';
  infos +='while not depend so much on copper cross section.\n';
  infos +=' \n';
  infos +='If you repeatedly work from a specific default setting ( or even multiple sets )\n';
  infos +='it makes sense to store that in the project settings ( drop down selector )\n';
  infos +='where you find 3 (default / small / big ) projects prepared,\nso now easy to store presettings of even more as 3 typicals\n';  
  infos +=' \n';  
  infos +='in biorock.html header line is a "(translate)" link to google translate english spanish...\n';
  infos +='this link is hardcoded to this repository\n';
  infos +=' \n';
  infos +='end of user install info, please disable later by variable diagSetup at sketch_biorock.js\n';
  infos +=' \n';
  
  dss = ''; //_ clean data sheet STEEL
  dss +=' \n';
  dss +='## BIOROCK DataSheet_';
  justnow = ''+year()+'_'+nf(month(),2)+'_'+nf(day(),2)+'-'+nf(hour(),2)+':'+nf(minute(),2)+':'+nf(second(),2);
  dss +=justnow+'\n';  
  dss +=' \n';
  dss +='## TUNNEL DESIGN:\n';
  dss +='number of arcs / arcN _________________ '+nf(arcN,0,0)+'\n';
  dss +='distance between arcs / arc_dist ______ '+nf(arc_dist,0,2)+' m\n';
  dss +='calculated / tunnellength _____________ '+nf(tunnellength,0,2)+' m\n';
  dss +='set tunnel height / archeight _________ '+nf(archeight,0,2)+' m\n';
  dss +='and tunnel width / arcbase ____________ '+nf(arcbase,0,2)+' m\n';
  if ( help ) {
    dss +='_ use some calculated helping variables\n';
    dss +='_ arcrad:'+ nf(arcrad,0,1)+' m X= arcrad-archeight '+ nf(X,0,1)+' m\n';
    dss +='_ arcPHI: '+ nf(arcPHI,0,1)+' rad or '+ nf(arcPHI*180/PI,0,1)+' deg\n';
  }
  dss +='calculated / arclength ________________ '+nf(arclength,0,2)+' m\n';
  dss +=' \n';  
  dss +='## BILL OF MATERIAL STEEL:\n';
  dss +='use building steel rods _______________ '+nf(roddiam*1000,0,1)+' mm diameter\n';
  dss +='for the arcs need:\n';
  dss +=' '+nf(arcN,0,0)+' times '+nf(arclength,0,2)+' m, total: _______________ '+nf(arcN * arclength,0,2)+' m\n';
  dss +='for a square grid on the arcs need:\n';
  dss +=' '+nf(longsN,0,0)+' times '+nf(tunnellength,0,2)+' m, total: _______________ '+nf(longsN * tunnellength,0,2)+' m\n';
  dss +='( so the real distance between the long rebars is '+nf(arclength/(longsN-1),0,2)+',\nwhile the arc distance is '+nf(arc_dist,0,2)+' )\n';
  dss +='every 3rd arc make a inner support for the tunnel:\n';
  dss +='arc baseline:\n';
  dss +=' '+nf(arcbaseN,0,0)+' times '+nf(arcbase,0,2)+' m, total: ________________ '+nf(arcbaseN * arcbase,0,2)+' m\n';
  
  // after drawing and calculation ( rodm total ) this is the 3rd time we do this calculation ( bad structural code design... )
  let topN =  int(longsN/2.0);
  dss +='we need from baseline up to arc ( weld points ) also steel \n( again every 3rd gridpoint ) total '+nf(arcbaseN,0,0)+' times\n';
  let  arcsupm =0.0;
  for (let i = 0; i < arcN; i++) {
    if (i % 3 == 0) {
      if ( i == 0 ) dss +='arc '+(i+1)+'\n';
      for ( let n = 0; n < topN; n+=3 ) {
        let wpv0 = awps[topN + i * longsN + n]; //______________________ get single vectors again /  actually only first arc data would suffice
        if ( i == 0 ) dss +='at x: '+nf(wpv0.x,0,2)+' hight ______________________ '+nf(wpv0.y*-1.0,0,2)+' m\n';
        arcsupm += wpv0.y*-1.0;
        if ( n > 0 ) { //_______________________________________________ if not middle do it again to the right
          let wpv0 = awps[topN + i * longsN - n]; //____________________
          if ( i == 0 ) dss +='at x: '+nf(wpv0.x,0,2)+ ' hight _____________________ '+nf(wpv0.y*-1.0,0,2)+' m\n';
          arcsupm += wpv0.y*-1.0;
        }
      }
    }
  }
  dss +=' \ntotal steel up to arc _________________ '+nf(arcsupm,0,1)+' m\n';
  dss +='here more material / calculation needed...\n';
  
  dss +=' \n';
  dss +='## STEEL SUMMARY:\n';
  dss +='total rod steel length /rodm: _________ '+nf(rodm,0,2)+' m\n';
  dss +='total rod steel surface /rodA: ________ '+nf(rodA,0,2)+' m2\n';
  dss +='total rod steel volume /rodV: _________ '+nf(rodV,0,3)+' m3\n';
  dss +='total tunnel weight /rodM: ____________ '+nf(rodM,0,2)+' kg\n';
  steel_euro = rodM*euro_steel; //______________________________________ steel kg price
  dss +='with '+nf(euro_steel,0,2)+'EUR per kg total:_____________ '+nf(steel_euro,0,2)+' EUR steel costs\n';
  
  dss +=' \n';

  // ELECTRIC
  //____________________________________________________________________ electric calcs
  if ( e_PS_A_overwrite )  { 
    console.log('MANUAL OVERWRITE') 
  } else {
    e_PS_A_sec = rodA * e_bamp; //__________________________________________ tunnel ampere
    if (e_public) inp21.value(str(e_PS_A_sec)); //__________________________ show it
  }
  e_anode_cable = p_PS_beach + p_beach_struct + p_struct_anode;
  e_cathode_cable = p_PS_beach + p_beach_struct;
  e_DC_cable = e_anode_cable + e_cathode_cable; //______________________ total cable length
  
  if (e_public) inp25.value(e_DC_cable); //_____________________________ show it
  // for some historic biorock projects ( medium DOME or TUNNEL ) we found from datacollection
  // that a design calculation might work using our second BIOROCK OHM model:
  // Biorock_Volt = a + m * Biorock_Amp
  // but to calculate the power supply you need to add the voltage/power losses on the DC cable too
  
  e_cableA = PI * e_copper_diam * e_copper_diam / 4.0; //_______________ m2 wire cross section
  e_copper_rho = e_copper_rho20*((e_copper_T-20)*e_copper_alpha +1 ); // temperature compensated copper Ωm
  e_cableR = e_DC_cable * e_copper_rho / e_cableA; //___________________ total cable resistance Ω
  //____________________________________________________________________ calculate the voltage by Kirchhoffs circle law
  // make MODEL variable and selectable
  // old: e_biorock_V = 2.10 + 0.20 * e_PS_A_sec; //____________________________ biorock ohm model estimate for small projects only, very much anode depending
  e_biorock_V = e_Model_V + e_Model_Ohm * e_PS_A_sec; //____________________________ biorock ohm model estimate for small projects only, very much anode depending
  e_cableV = e_cableR * e_PS_A_sec;
  e_PS_V_sec = e_biorock_V + e_cableV; //_______________________________ so need a driving voltage from power supply
  if (e_public) inp22.value(e_PS_V_sec); //_____________________________ show it
  e_biorock_W = e_biorock_V*e_PS_A_sec;
  e_PS_W_sec = e_PS_V_sec * e_PS_A_sec;
  e_PS_W =  e_PS_W_sec * 100 / e_PS_eff; //_____________________________ incl. estimated losses in PS
  e_PS_A = e_PS_W  / e_PS_V;
  if (e_public) inp23.value(e_PS_W); //_________________________________ show it
  e_PS_total_eff = e_biorock_W / e_PS_W; //_____________________________ system efficiency


  dse=""; //___ clean data sheet ELECTRIC
  dse +='## ELECTRIC SCOPE:\n';
  dse +='biorock accretion setting ampere DC per squaremeter steel surface:\n';
  dse +='here / e_bamp _________________________ '+nf(e_bamp,0,2)+' A/m2\n';
  if ( e_PS_A_overwrite ) dse +='manual current overwrite testcalc: ';
  dse +='so must drive DC amp / e_PS_A _________ '+nf(e_PS_A_sec,0,2)+' A\n';
  dse +=' \n';
  dse +='## BILL OF MATERIAL CABLE:\n';
  dse +='from beach PS using a one wire cable to anode and one to cathode / steel BIOROCK structure / \n';
  dse +='with a copper diameter '+nf(e_copper_diam*1000,0,2)+' mm / cross section of '+nf(e_cableA*1000000,0,2)+' mm2\n';
  dse +='beach PS to anode _____________________ '+nf(e_anode_cable,0,1)+' m ( '+p_PS_beach+' + '+p_beach_struct+' + '+p_struct_anode+' )\n';
  dse +='beach PS to tunnel ____________________ '+nf(e_cathode_cable,0,1)+' m ( '+p_PS_beach+' + '+p_beach_struct+' )\n';
  dse +='total DC cable ________________________ '+nf(e_DC_cable,0,1)+' m\n';
  cable_euro = euro_cable * e_DC_cable * e_cableA*1000000;
  dse +='with '+nf(euro_cable,0,2) +' EUR/m/mm2 ___________________ '+nf(cable_euro,0,2)+' EUR cable costs\n';
  dse +='\n';
  dse +='that cable has a resistance of ________ '+nf(e_cableR,0,2)+' ohm ( at '+nf(e_copper_T,0,1)+' degC copper temperature )\n';
  dse +=' \n';
  dse +='## POWER SUPPLY ESTIMATE:\n';
  dse +='resulting in a DC cable voltage drop __ '+nf(e_cableV,0,2)+' V\n';
  dse +='add a estimated Biorock voltage drop __ '+nf(e_biorock_V,0,2)+' V\n';
  if ( help ) {
  dse +=': BIOROCK Model : \n';
  dse +='from the old BIOROCK projects there is not much of a electric data collection.\n';
  dse +='best is data like Volt and Amp measured on the output of a beach power supply.\n';
  dse +=' \n';
  dse +='only where add info like cable length and wire size is available a back calculation of a\n';
  dse +='"BIOROCK resistance" is possible. \n';
  dse +='historically the PS was a ( transformer + rectifier ) battery charger, about ?Vo 14VDC? \n';
  dse +='and for the projects where i collected data a average of 0.33 ohm for BIOROCK process was suggested.\n';
  dse +='now with the use of battery charger with voltage dials,\nand later switching PS or even adjustable laboratory PS\n';
  dse +='but also with the understanding that it is better to see it as a battery loading process \n';
  dse +='( back voltage and back current measurements ) instead as a mere resistor\n';
  dse +='a process identification in form of a linear regression ( spline ) fits better to the test data\n';
  dse +='and the electro chemical understanding.\n';
  dse +='for a working range 5 .. 20 A\nwith a steel cathode about 7.5m2 reactive surface to water\n';
  dse +='and with this a target of 15A for the project for best accretion, ( for first 3 years setup )\n';
  dse +='you selected model: '; //________________________________________________________________________ rev 0.9.7.4
  if ( e_model == 0 ) dse += 'mesh anode\n';
  if ( e_model == 1 ) dse += 'ribbon anode\n';
  dse +='BIOROCK Volt = '+nf(e_Model_V,0,2)+'[V] + '+nf(e_Model_Ohm,0,2)+'[ohm] * PS[A]\n';
  dse +='what seemed to work well esp. also for BOLPS projects.\n';
  dse +='But recent project info regarding used anode size / material showed this\n';
  dse +='( copper cable - anode connection, anode internal resistance, anode surface to water resistance\n';
  dse +='incl. electrolysis O2... production there) is a critical path.\n';
  dse +='upto now there is no data set regarding anode size and distance what would enable us to factor it into the model.\n';
  dse +=' \n';
  }
  dse +='so requires a PS voltage of ___________ '+nf(e_PS_V_sec,0,2)+' V\n';
  dse +='at that current of ____________________ '+nf(e_PS_A_sec,0,1)+' A\n';
  dse +='estimated Biorock tunnel module power requirement\n';
  dse +='at this working point need ____________ '+nf(e_PS_W,0,0)+' W\n';
  e_PS_Wa = e_PS_W * 365 * 24 / 1000; //________________________________ power in a year
  GRID_euro = euro_kWh * e_PS_Wa; //____________________________________ GRID power costs
  dse +='or need per year ______________________ '+nf(e_PS_Wa,0,0)+' kWh \n';
  dse +='with '+nf(euro_kWh,0,2)+' EUR/kWh power costs per year_ '+nf(GRID_euro,0,2)+' EUR/a\n';
  dse +='with GRID power:_______________________ '+nf(e_PS_V,0,0)+'VAC and '+nf(e_PS_A,0,2)+'A\n';
  dse +='using a switching-PS efficiency of ____ '+nf(e_PS_eff,0,1)+'%\n';
  dse +='for the spec of the PS pls. consider to use the max working point \n';
  dse +='at about 80% of PS nominal load.\n';
  dse +=' \n';
  dse +='please note the overall efficiency of your setup \n( mainly regarding too small / but long cable.. )\n';
  dse +='the BIOROCK process uses / gets only __ '+nf((e_biorock_W),0,0)+' W of the total '+nf(e_PS_W,0,0)+' W,\nthat would be a total efficiency of ___ '+nf((e_PS_total_eff*100.0),0,0)+' % \n';  
  dse +=' \n';
  if ( help ) {
    dse +='in any case, best would be to have a PS what drives / controls that current, instead of the voltage,\n';
    dse +='even when water -temperature, -salinity, ( newmoon )-tide OR structure accretion varies,\n';
    dse +='also for startup, limiting current while capacitive loading, and very first structure de-rosting;\n';
    dse +='even better with a manual adjustable current setpoint from beach.\n';
    dse +=' \n';
  }
  dse +='*** \n';

  if ( bolps_enable ) {
    bolps();
    dse +=bolpsds;
    dse +='*** \n';
    dse +=': please use key :\n';
  } else {
    // here now add the ascii-circuit-drawing ( like in bolps )
    // now experiment here with ascii art / graphic electric circuit in HTML with using <pre><code>
    dse +='``` \n';
    dse +='| GRID    BEACH PS              CABLE                                      BIOROCK   \n';
    dse +='|         ____________                                                               \n';
    dse +='| ________| AC       |__________[ R ]___________________________________[   ANODE   ]\n';
    dse +='| ________|      DC  |__________________________________________________[ STRUCTURE ]\n';
    dse +='|         |__________|                                                               \n';
    dse +='|                                                                                    \n';
    dse +='|            '+nf(e_PS_eff,0,1)+'%              '+nf(e_cableR,0,2)+'ohm                                     total: '+nf(e_PS_total_eff*100,0,1)+'%\n';
    dse +='|    '+nf(e_PS_W,3,1)+'W          '+nf(e_PS_W_sec,3,1)+'W     '+nf(e_cableV*e_PS_A_sec,3,1)+'W                                      '+nf(e_biorock_W,3,1)+'W\n';
    dse +='|    '+nf(e_PS_V,3,1)+'V          '+nf(e_PS_V_sec,3,1)+'V     '+nf(e_cableV,3,1)+'V                                      '+nf(e_biorock_V,3,1)+'V\n';
    dse +='|    '+nf(e_PS_A,3,1)+'A          '+nf(e_PS_A_sec,3,1)+'A                            \n';
    dse +='|                                                                                    \n';
    dse +='``` \n'; // end pre code

    dse +=': please use key :\n';
    dse +='[b] to see here a other optional electric design\n';

  }
  dse +='[d] to download / save settings text file or [ctrl/cmd][p] for browser print ( like to PDF )\n';    
  dse +=' \n';    
  // create a combined data string as documentation
  ds=""; //___ clean
  ds +=' \n';
  ds += info; //________________________________________________________ add the above operational concept info  
  if ( diagSetup && help ) { 
    ds += infos; //______________________________ add the above setup info // disable later diagSetup
    ds +='*** \n';
  }
  ds += dss; //_________________________________________________________ add data sheet STEEL
  if (e_public) ds += dse; //___________________________________________ add data sheet ELECTRIC
  ds +='*** \n'; //_____________________________________________________ add a <hr> via markdown
  ds +='have fun using this tool, powered by [Processing P5.js](https://p5js.org/)\n';
  ds += revv+'\n'+revr+'\n'; //________________________________________ revision variables see sketch_biorock.js
  ds += copyleft+'\n';
  ds +=' \n';
  
  if (diagc) console.log(ds); //________________________________________ can show at console too / but who knows how to read that?
  
  if (diaghtml) {
    let docs = md.render(ds); //________________________________________ use new markdown lib in MOD3b
    docp.html(docs); //___ show it in html page
  }
  // as the long text at end of page conflicts with some browser / processing keyboard operation test a 3rd. way: auto download here at end of calc
  print_DataGrid(); // newest data grid to string
  ds = infod + ds; // print for download the spreadsheet area first
  dllist = split(ds, '\n'); //__________________________________________ make new lines to array
  if ( autodl ) saveStrings(dllist, 'BIOROCK_datasheet_'+justnow+'.txt'); //___ else can use key [d] for manual download
}

// change way again, for steel we do a extra calculation and separate document function, for electric combine first calculate then document in one function ( above )
// now i try calc and document value / sentence by sentence ? better readable ?

function bolps() {
  bolpsds=''; //__ clear
  bolpsds +='| now you could use more cable ( bigger copper diameter ) to reduce the electric cable losses, or\n\n';
  if ( help ) {
    bolpsds +='## | BIOROCK history lesson:\n';
    bolpsds +='| change the concept: it is better to run on the same cable a higher voltage \n| / lower current / with much lower losses /\n';
    bolpsds +='| and use a secondary power converter near structure or anode.\n';
    bolpsds +='| \n';
    bolpsds +='| Biorock Thailand maintenance crew: TPS, Roby and me- KLL\n| repaired a Biorock powersupply ( old-fashioned battery charger )\n';
    bolpsds +='| on the island Koh Tau and at restart i try to get electric readings for my back-calc,\n| but failed as the info about used cable .. length was missing.\n';
    bolpsds +='| on the way home on the back of a pickup truck\n| we talked about that cable power-losses ( R * I * I )\n';
    bolpsds +='| and there i had the idea to use a underwater power supply\n| ( near BIOROCK anode or cathode/structure ) \n';
    bolpsds +='| and use a higher voltage on the cable going down to the structure PS,\n';
    bolpsds +='| projects with using 240 or 120 VAC for safety reasons had installed\n| isolation transformer and earth leakage current detectors.\n';
    bolpsds +='| TPS named it BOLPS: Biorock On Location Power Supply\n';
    bolpsds +='| as it basically is like we moved the PS down into the water, we can call it GRID BOLPS.\n';
    bolpsds +='| But after problems with the building permit the concept was changed again,\n';
    bolpsds +='| to SELV Safe Extra Low Voltage(certified) 48 VDC beach power supply\n';
    bolpsds +='| and a DC/DC converter in the BOLPS, but with 48 V the efficiency increase is limited.\n';
    bolpsds +='| we could call it SELV BOLPS\n';

    bolpsds +='## | GRID BOLPS\n'; 
    bolpsds +='| if we are allowed to use GRID AC feed down to a BOLPS \n';
    bolpsds +='| then we would have much less losses on the beach cable ( VDC 48 v.v. 230VAC ) \n';
    bolpsds +='| and also the beach PS losses not occure, but we have to expect more losses inside the BOLPS\n';


  //  bolpsds +='|\n';
  //  bolpsds +='| just yesterday i see a webpage ( of BIOROCK-?friends?competition? ) what also want to use exactly that concept\n'; // http://www.vicorpower.com/press-room/ccell-coral-reef
  //  bolpsds +='|\n';
  
    bolpsds +='| \n';
  }
  bolpsds +='## | Biorock OnLocation Power Supply\n'; 
  bolpsds +='| in case we use BOLPS the first thing is that the beach cable\n| and the BOLPS cable can have different size:\n';
  bolpsds +='| so to drive the BIOROCK current we use '+nf(e_BOLPS_diam*1000.0,0,2) +' mm diameter wire ( fix here ) \n';
  bolpsds +='| and the cable has the length ( structure anode distance setting ) '+nf(p_struct_anode,0,1)+' m\n| and add '+nf(e_BOLPS_DC_cathode_cable,0,0)+'m fix for cathode connection\n';
  e_BOLPS_DC_cable = e_BOLPS_DC_cathode_cable + p_struct_anode;
  bolpsds +='| BOLPS cable total: ____________________ '+nf(e_BOLPS_DC_cable,0,2)+' m\n';
  e_BOLPS_cableA = PI * e_BOLPS_diam * e_BOLPS_diam / 4.0; //___________ m2 wire cross section
  bolpsds +='| with a cross section of _______________ '+nf(e_BOLPS_cableA*1000000,0,2)+' mm2\n';
  cable_BOLPS_euro = e_BOLPS_DC_cable * e_BOLPS_cableA * 1000000 * euro_cable; //_ BOLPS cable only
  bolpsds +='| with '+nf(euro_cable,0,2) +' EUR/m/mm2 ___________________ '+nf(cable_BOLPS_euro,0,2)+' EUR BOLPS cable costs\n';  
  e_BOLPS_cableR = e_BOLPS_DC_cable * e_copper_rho / e_BOLPS_cableA; //_ total cable resistance Ω
  bolpsds +='| what has a DC cable resistance of _____ '+nf(e_BOLPS_cableR,0,2)+' ohm ( at '+nf(e_copper_T,0,1)+' degC copper temperature )\n';
  e_BOLPS_cableV = e_BOLPS_cableR * e_PS_A_sec;
  bolpsds +='| causing a BOLPS cable voltage drop of _ '+nf(e_BOLPS_cableV,0,2)+' V\n';
  bolpsds +='| add a estimated Biorock voltage drop __ '+nf(e_biorock_V,0,2)+' V\n';
  e_BOLPS_PS_V = e_biorock_V + e_BOLPS_cableV; //____________________________________ so need a driving voltage from power supply
  bolpsds +='| so requires a BOLPS PS voltage of _____ '+nf(e_BOLPS_PS_V,0,2)+' V\n';
  bolpsds +='| at that current of ____________________ '+nf(e_PS_A_sec,0,1)+' A\n';
  bolpsds +='| estimated BOLPS power requirement\n';
  e_BOLPS_PS_W = e_BOLPS_PS_V * e_PS_A_sec / e_BOLPS_eff; //_________________________ incl. estimated losses in BOLPS PS fix efficiency
  bolpsds +='| at this working point need ____________ '+nf(e_BOLPS_PS_W,0,0)+' W\n| incl. estimated losses in BOLPS DC/DC PS use fix '+nf(100*e_BOLPS_eff,0,2)+' % efficiency\n';
  bolpsds +='| \n';
  if ( GRID_BOLPS ) {
    bolpsds +='| on the beach we now would have that '+nf(e_BOLPSSELV_V,0,0)+'VAC to '+nf(e_BOLPSSELV_sec_V,0,0)+'VAC FEEDER\n';
    bolpsds +='| and new beach cable length of _________ '+ nf(e_DC_cable - p_struct_anode,0,1)+' m,\n';
  } else {
    bolpsds +='| on the beach we now would have that '+nf(e_BOLPSSELV_V,0,0)+'VAC to SELV (certified) '+nf(e_BOLPSSELV_sec_V,0,0)+'VDC power supply.\n';
    bolpsds +='| and new beach cable length of _________ '+ nf(e_DC_cable - p_struct_anode,0,1)+' m,\n';
  }
  e_BOLPSSELV_cableR=( e_DC_cable -  p_struct_anode) * e_copper_rho / e_cableA;
  bolpsds +='| with resistance of ____________________ '+nf(e_BOLPSSELV_cableR,0,2)+' ohm\n';
  cable_BOLPSSELV_euro = (e_DC_cable - p_struct_anode) * e_cableA * 1000000 * euro_cable; //_ beach cable 
  bolpsds +='| with '+nf(euro_cable,0,2) +' EUR/m/mm2 ___________________ '+nf(cable_BOLPSSELV_euro,0,2)+' EUR beach cable costs\n';
  bolpsds +='| \n';
  if ( help ) {
    bolpsds +='| that cable voltage drop need to be considered, if you check the code,\n| see here comes some heavy stuff ( quadratic equation ) to calculate the current in the '+nf(e_BOLPSSELV_sec_V,0,0)+'VDC loop:\n';
  }
// KIRCHHOFF's Voltage Law						Uselv = Ur + Ui				
// OHM's Law					              	Ur = Rr * Ai				
// from POWER	backcalc               	Ui = Pi / Ai				
//
// Uselv -Rr*Ai - Pi/Ai = 0				https://en.wikipedia.org/wiki/Quadratic_equation	
//  in normal form
// Ai*Ai -Uselv*Ai/Rr +Pi/Rr =0
// 
// p = - Uselv/Rr     
// q = Pi/Rr    
// Ai1,2 = - p/2 +- sqrt( sq( p/2 ) - q )
// with a problem if sq(p/2) smaller q

  let p = -1.0 * e_BOLPSSELV_sec_V / e_BOLPSSELV_cableR;
  let q = e_BOLPS_PS_W / e_BOLPSSELV_cableR;
  e_BOLPSSELV_sec_A = - (p/2.0) - sqrt( sq(p/2.0) - q ); //_____________ use Ai2 solution
  bolpsds +='| and intermediate current ______________ '+nf(e_BOLPSSELV_sec_A,0,2)+' A\n';
  e_BOLPSSELV_cableV = e_BOLPSSELV_sec_A * e_BOLPSSELV_cableR;
  bolpsds +='| voltage drop on intermediate cable ____ '+nf(e_BOLPSSELV_cableV,0,2)+' V\n';
  e_BOLPS_PS_inV = e_BOLPS_PS_W / e_BOLPSSELV_sec_A;
  bolpsds +='| voltage drop on BOLPS input ___________ '+nf(e_BOLPS_PS_inV,0,2)+' V\n';
  e_BOLPSSELV_sec_W = e_BOLPSSELV_sec_V * e_BOLPSSELV_sec_A;
  bolpsds +='| beach PS output power _________________ '+nf(e_BOLPSSELV_sec_W,0,1)+' W\n';
  bolpsds +='| GRID side:\n';
  e_BOLPSSELV_W = e_BOLPSSELV_sec_W / e_BOLPSSELV_eff;
  e_BOLPSSELV_Wa = e_BOLPSSELV_W * 365 * 24 / 1000;
  GRID_BOLPS_euro = e_BOLPSSELV_Wa * euro_kWh;
  bolpsds +='| beach PS input power __________________ '+nf(e_BOLPSSELV_W,0,1)+' W at a efficiency of '+nf(100*e_BOLPSSELV_eff,0,2)+' %\n';
  bolpsds +='| or need per year ______________________ '+nf(e_BOLPSSELV_Wa,0,0)+' kWh \n';
  bolpsds +='| with '+nf(euro_kWh,0,2)+' EUR/kWh power costs per year_ '+nf(GRID_BOLPS_euro,0,2)+' EUR/a\n';
  e_BOLPSSELV_A = e_BOLPSSELV_W / e_BOLPSSELV_V;
  bolpsds +='| with GRID power:_______________________ '+nf(e_BOLPSSELV_V,0,0)+'VAC and '+nf(e_BOLPSSELV_A,0,2)+'A\n';
  e_BOLPSSELV_total_eff = e_biorock_W / e_BOLPSSELV_W;
  bolpsds +='| so with BOLPS could have a overall efficiency of '+nf(100*e_BOLPSSELV_total_eff,0,2)+' %\n';
  bolpsds +='| \n';
// now experiment here with ascii art / graphic electric circuit in HTML with using <pre><code>
  bolpsds +='``` \n';
  bolpsds +='| GRID    BEACH PS              CABLE                                      BIOROCK   \n';
  bolpsds +='|         ____________                                                               \n';
  bolpsds +='| ________| AC       |__________[ R ]___________________________________[   ANODE   ]\n';
  bolpsds +='| ________|      DC  |__________________________________________________[ STRUCTURE ]\n';
  bolpsds +='|         |__________|                                                               \n';
  bolpsds +='|                                                                                    \n';
  bolpsds +='|            '+nf(e_PS_eff,0,1)+'%              '+nf(e_cableR,0,2)+'ohm                                     total: '+nf(e_PS_total_eff*100,0,1)+'%\n';
  bolpsds +='|    '+nf(e_PS_W,3,1)+'W          '+nf(e_PS_W_sec,3,1)+'W     '+nf(e_cableV*e_PS_A_sec,3,1)+'W                                      '+nf(e_biorock_W,3,1)+'W\n';
  bolpsds +='|    '+nf(e_PS_V,3,1)+'V          '+nf(e_PS_V_sec,3,1)+'V     '+nf(e_cableV,3,1)+'V                                      '+nf(e_biorock_V,3,1)+'V\n';
  bolpsds +='|    '+nf(e_PS_A,3,1)+'A          '+nf(e_PS_A_sec,3,1)+'A                            \n';
  bolpsds +='|                                                                                    \n';
  bolpsds +='|                                                                                    \n';
  bolpsds +='|                                                                                    \n';
  if ( GRID_BOLPS ) {
    bolpsds +='| GRID    BEACH FEEDER          CABLE         BOLPS              CABLE     BIOROCK   \n';
    bolpsds +='|         ____________                       ____________                            \n';
    bolpsds +='| ________| AC       |__________[ R ]________| AC       |________[ R ]__[   ANODE   ]\n';
    bolpsds +='| ________|      AC  |_______________________|       DC |_______________[ STRUCTURE ]\n';
  } else {
    bolpsds +='| GRID    BEACH PS              CABLE         BOLPS              CABLE     BIOROCK   \n';
    bolpsds +='|         ____________                       ____________                            \n';
    bolpsds +='| ________| AC       |__________[ R ]________| DC       |________[ R ]__[   ANODE   ]\n';
    bolpsds +='| ________|      DC  |_______________________|       DC |_______________[ STRUCTURE ]\n';
  }
  bolpsds +='|         |__________|                       |__________|                            \n';
  bolpsds +='|                                                                                    \n';
  bolpsds +='|            '+nf(e_BOLPSSELV_eff*100,0,1)+'%              '+nf(e_BOLPSSELV_cableR,0,2)+'ohm          '+nf(e_BOLPS_eff*100,0,1)+'%           '+nf(e_BOLPS_cableR,0,2)+'ohm    total: '+nf(e_BOLPSSELV_total_eff*100,0,1)+'% \n';
  bolpsds +='|    '+nf(e_BOLPSSELV_W,3,1)+'W          '+nf(e_BOLPSSELV_sec_W,3,1)+'W     '+nf(e_BOLPSSELV_cableV*e_BOLPSSELV_sec_A,3,1)+'W   '+nf(e_BOLPS_PS_W,3,1)+'W         '+nf(e_BOLPS_PS_W*e_BOLPS_eff,3,1)+'W   '+nf(e_BOLPS_cableV*e_PS_A_sec,3,1)+'W     '+nf(e_biorock_W,3,1)+'W\n';
  bolpsds +='|    '+nf(e_BOLPSSELV_V,3,1)+'V          '+nf(e_BOLPSSELV_sec_V,3,1)+'V     '+nf(e_BOLPSSELV_cableV,3,1)+'V   '+nf(e_BOLPS_PS_inV,3,1)+'V         '+nf(e_BOLPS_PS_V,3,1)+'V   '+nf(e_BOLPS_cableV,3,1)+'V     '+nf(e_biorock_V,3,1)+'V\n';
  bolpsds +='|    '+nf(e_BOLPSSELV_A,3,1)+'A          '+nf(e_BOLPSSELV_sec_A,3,1)+'A                             ' +nf(e_PS_A_sec,3,1)+'A \n';
  bolpsds +='|                                                                                \n';
  bolpsds +='``` \n'; // end pre code
  if ( help ) {
    bolpsds +='| a google spread sheet with similar calculation i did [here](https://docs.google.com/spreadsheets/d/1hgasQUpGNY2VDT27Jdus3ygX51Gic_MpCwbkact9KCI/edit?usp=sharing) \n';
    bolpsds +='|                                                                                \n';
    bolpsds +='| also here can try a other comparison of the two electric powering ways, by the costs:\n';
  }
  bolpsds +='| as a key number use the cable costs plus the power costs summed up over 3 years. \n';
  bolpsds +='| ( but ignoring the differences in electric PS devices and installation costs )\n';
  bolpsds +='| no BOLPS:   '+nf(3 * GRID_euro + cable_euro,0,0)+' EUR\n';
  bolpsds +='| with BOLPS: '+nf(3 * GRID_BOLPS_euro + cable_BOLPS_euro + cable_BOLPSSELV_euro,0,0)+' EUR\n';
  bolpsds +='|                                                                                \n';

}

