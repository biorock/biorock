document.oncontextmenu = function() {
  return false;
}

//_____________________________________ PTZ tab // KLL new init Xmag 4 Ymag 1 Zmag 8 for this project only
let mode = 0;
let Zmag = 8;
let dz = 0.5; // adjust zoom for CHROME browser ( mouse wheel and [PAGE UP DOWN] and [-][+]
let Zaxis=-30;                                                       
let Xmag=4, Ymag = 1;
let newXmag=0, newYmag=0, newZmag=0;
let newxpos=0, newypos = 0;       // for PAN
let xposd=0, yposd = 0;           // for PAN
let diagp = false;//true;//false;
let usekey = false;//true
let tablet = true;//false //_______________________________________ make android friendly sliders for PTZ operation as mouse scroll or key operation not work on touch screens
var slX_val = 0, slY_val =0, slZ_val = 10;

function PTZ_sliders() {
  // read slider for new tablet operation 
  let tX = slX.value();
  let tY = slY.value();
  let tZ = slZ.value();
  if ( tX != slX_val ) {
    slX_val = tX; 
    if (diagp) console.log(' slX_val ',slX_val);
    Xmag = -1.0*radians(slX_val);
  }
  if ( tY != slY_val ) {
    slY_val = tY; 
    if (diagp) console.log(' slY_val ',slY_val);
    Ymag = -1.0*radians(slY_val);
  }
  if ( tZ != slZ_val ) {
    slZ_val = tZ; 
    if (diagp) console.log(' slZ_val ',slZ_val);
    Zmag = slZ_val;
  }
}

//_________________________________________________________________ ROTATE / TILDE and MOVE / PAN
function mousePressed() {
  if ( mouseX > 0 && mouseX < 800 && mouseY > 0 && mouseY < 500 ) { // allow IN CANVAS only to not mix with DOM elements
    if      (mouseButton == LEFT)  mode=1;     // ORBIT
    else if (mouseButton == RIGHT) mode=2;     // PAN
  }
}

//_________________________________________________________________ mouse PT end
function mouseReleased() { 
  mode = 0;
}

//_________________________________________________________________ mouseWheel ZOOM
function mouseWheel(event) {
  if ( mouseX > 0 && mouseX < 800 && mouseY > 0 && mouseY < 500 ) { // allow IN CANVAS only to not mix with DOM elements
    if (diagp) print(' mouse wheel ',event.delta); // looks like browsers see different values, now check
    Zmag += event.delta*dz/100;
    if (diagp) print("Zmag: "+nf(Zmag, 1, 2));
    return false;
  }
}

//_________________________________________________________________ key up down left right Page up page Down
function keyPressed() {
  if ( usekey ) {
    if ( keyCode == UP_ARROW   )  Ymag -= 0.1 ;
    if ( keyCode == DOWN_ARROW )  Ymag += 0.1 ;
    if ( keyCode == RIGHT_ARROW)  Xmag -= 0.1 ;
    if ( keyCode == LEFT_ARROW )  Xmag += 0.1 ;
    if ( keyCode == 33 || key == '-' )    Zmag -= dz ;   // page UP [-]
    if ( keyCode == 34 || key == '+' )    Zmag += dz ;   // page DOWN [+]
  }
  if ( key == 'i' ) console.log("windowWidth " + windowWidth + " windowHeight " + windowHeight + " width " + width + " height " + height);
  if ( key == 's' ) {
    show_land = ! show_land;
    inp06.value(str(show_land)); // write back    
  }
  if ( key == 'a' ) {
    show_steel = ! show_steel;
    inp16.value(str(show_steel)); // write back
  }
  if ( key == 'o' ) tunnelswitch = ! tunnelswitch;
//  if ( key == 'd' ) autodl = ! autodl;
  if ( key == 'd' ) if ( ! autodl ) saveStrings(dllist, 'BIOROCK_datasheet_'+justnow+'.txt'); //if NOT auto download ( after change ) can ask manually for download 
  if ( key == 'b' ) { 
    bolps_enable = ! bolps_enable;
    calc_list();
  }
  if ( key == 'h' ) {
    help = ! help; // show verbose info in HTML...
    calc_list(); // update
  }
  if (diagp) print("key: "+key+" keyCode: "+keyCode);
}
//_________________________________________________________________ Pan Tilde Zoom
function PTZ() {
  if (tablet) PTZ_sliders();
  push(); 
  translate(0,0, Zaxis);                          // WEBGL centers automatic
  if ( mode == 2 ) {                              // PAN ( right mouse button pressed)
    xposd = (mouseX-float(width/2));
    yposd = (mouseY-float(height/2));
  }  
  newxpos = xposd;// xposd = 0;
  newypos = yposd;// yposd = 0;
  translate(newxpos, newypos, 0);          // move object
  if ( mode == 1 ) {  // ORBIT ( left mouse button pressed)
    newXmag = mouseX/float(width) * TWO_PI;
    newYmag = mouseY/float(height) * TWO_PI;
    let diff = Xmag-newXmag;
    if (abs(diff) >  0.01)   Xmag -= diff/4.0;
    diff = Ymag-newYmag;
    if (abs(diff) >  0.01)   Ymag -= diff/4.0;
    //console.log('mode ',mode,' mx ',mouseX,' my ',mouseY);
  }
  // console.log('Xmag ',Xmag ,'Ymag',Ymag,'Zmag', Zmag); // start view this project 4,1,8
  rotateX(-Ymag);   
  rotateY(-Xmag);   
  scale(Zmag);
  draw_object();                                // THE OBJECT  see main tab
  pop();
}


function axis() {
  push();
  stroke(200, 0, 0);
  box(100,4,4);
  ellipse(0, 0, height / 2, height / 2, 50);
  rotateX(HALF_PI);
  stroke(0, 200, 0);
  box(4,100,4);
  ellipse(0, 0, height / 2, height / 2, 50);
  rotateY(HALF_PI);
  stroke(0, 0, 200);
  box(100,4,4);
  ellipse(0, 0, height / 2, height / 2, 50);
  pop();
}
